import sys
import re
import unicodedata
import os
try:
    from num2words import num2words
except:
    sys.stderr.write("[WW] num2words not installed! Feature disabled. Please run: pip3 install num2words\n")

NUMBER= re.compile(r"-?([\d]+[\d.]*)")

class Processor:

    def __init__(self, exceptions, parse_special=False, num_to_words=None):
        self._exceptions = exceptions
        self._text_clean = ""
        self._parse_special = parse_special
        self._num_to_words = num_to_words
 
    def __process_token(self, s):
        x = ""
        bprevc = False
        c = ""
        i = 0
        while i < len(s):
            prevc = c
            c = s[i]
            cat = unicodedata.category(c)
            if cat[0] in ("L", "N", "M"):
                x += c.lower()
                bprevc = True
            elif c in self._exceptions:
                if not(bprevc) or i+1 == len(s):
                    bprevc = False
                    if x != "":
                        self._text_clean += "%s " % (x)
                    x = ""
                else:
                    if unicodedata.category(s[i+1])[0] in ("L", "N", "M"):
                        x += c
                        bprevc = True
                    else:
                        bprevc = False
                        if x != "":
                            self._text_clean += "%s " % (x)
                        x = ""
            else:
                bprevc = False
                if x != "":
                    self._text_clean += "%s " % (x)
                x = ""
            i += 1
        if x != "":
            self._text_clean += "%s " % (x)

    def process_line(self, t):
        self._text_clean = ""
        for w in t.split():
            if self._parse_special:
                if re.match("\[[^]]*\]", w) or re.match("<[^>]*>", w):
                    continue
            if self._num_to_words != None:
                if NUMBER.match(w):
                    try:
                        self._text_clean += "%s " % num2words(w, lang=self._num_to_words)
                    except:
                        self.__process_token(w)
                else:
                    self.__process_token(w)
            else:
                self.__process_token(w)
        return self._text_clean.strip()


if __name__ == "__main__":

    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--special-tokens", action="store_true", default=False, help="Parse special tokens (i.e. [music], [hesitation], <unk>, ...)")
    parser.add_argument("-n", "--num-to-words", default=None, help="Convert numbers to text, in the specified language (iso-639 code)")
    args = parser.parse_args()

    exceptions = ["-", "·", "'"]
    p = Processor(exceptions, parse_special=args.special_tokens, num_to_words=args.num_to_words) 

    for l in sys.stdin:
        print(p.process_line(l))

