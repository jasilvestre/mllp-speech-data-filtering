import sys
import json
import argparse
from TLKAlign import TLKAlign
from TLKSegment2 import TLKSegment as TLKSegment
from libdfxp import update_status, convert_dfxp

parser = argparse.ArgumentParser()
parser.add_argument("align")
parser.add_argument("scores")
parser.add_argument("out_prefix")
parser.add_argument("--write-stats-file", dest="stats_file", default=None)
parser.add_argument("--no-files", dest="generate_files", action="store_false")
parser.add_argument("--debug", action="store_true", default=False)
parser.add_argument("--sil-threshold", type=float, default=0.5)
parser.add_argument("--min-segment-length", type=float, default=0.5)
parser.add_argument("--max-segment-length", type=float, default=20)
parser.add_argument("--hlength-disable", dest="h_length", action="store_false")
parser.add_argument("--hlength-stdevs", type=float, default=2)
parser.add_argument("--hlength-max", type=float, default=1)
parser.add_argument("--hscore-disable", dest="h_score", action="store_false")
parser.add_argument("--hscore-factor", type=float, default=0.25)
parser.add_argument("--hscore-min", type=float, default=0)

args = parser.parse_args()

alignf = args.align
scoresf = args.scores
out_prefix = args.out_prefix

align = TLKAlign(silence_token="SP")
with open(alignf) as fd:
    align.read(fd)

with open(scoresf) as fd:
    align.import_scores(fd)

segment = TLKSegment()
segment.align = align

segment.build_training_segments(sil_thold=args.sil_threshold, min_seg_len=args.min_segment_length, max_seg_len=args.max_segment_length,
                                h_length=args.h_length, h_length_stdevs=args.hlength_stdevs, h_length_max=args.hlength_max, 
                                h_score=args.h_score, h_score_factor=args.hscore_factor, h_score_min=args.hscore_min, debug=args.debug)

if args.generate_files:
    dfxp = segment.to_dfxp()
    update_status(dfxp)
    trg_dfxp = "%s.dfxp" % out_prefix
    with open(trg_dfxp, 'w') as fd:
        fd.write(dfxp.toXml())
    
    srt = segment.to_srt()
    trg_srt = "%s.srt" % out_prefix
    with open(trg_srt, 'w') as fd:
        fd.write(srt)
    
    trg_json = "%s.json" % out_prefix
    with open(trg_json, 'w') as fd:
        fd.write(segment.to_json_string())

if args.stats_file:
    with open(args.stats_file, 'w') as fd:
        fd.write(json.dumps(segment.stats, indent=4, sort_keys=True))
else:
    print json.dumps(segment.stats, indent=4, sort_keys=True)

