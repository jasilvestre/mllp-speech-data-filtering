import json
import csv
import sys

if len(sys.argv) != 2:
    sys.stderr.write("%s <JSON_FILE>\n" % sys.argv[0])
    sys.exit(1)

jsonf = sys.argv[1]

with open(jsonf) as fd:
    l = json.load(fd)

csvwriter = csv.writer(sys.stdout)

for s in l:
    st = s['b']
    et = s['e']
    text = " ".join([ x['w'] for x in s['wl'] ])
    csvwriter.writerow([st, et, text])


