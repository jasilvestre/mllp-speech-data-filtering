import sys
import json
import argparse
from TLKAlign2 import TLKAlign
from TLKSegment2 import TLKSegment as TLKSegment
from libdfxp import update_status, convert_dfxp

parser = argparse.ArgumentParser()
parser.add_argument("align")
parser.add_argument("out_prefix")
parser.add_argument("--write-stats-file", dest="stats_file", default=None)
parser.add_argument("--no-files", dest="generate_files", action="store_false")
parser.add_argument("--debug", action="store_true", default=False)
parser.add_argument("--sil-threshold", type=float, default=0.5)
parser.add_argument("--max-segment-length", type=float, default=20)
parser.add_argument("--keep-segmentation", action="store_true", default=False, help="Do not re-segment; use recognition segmentation instead.")
parser.add_argument("--soft-boundaries", action="store_true", default=False, help="Consider soft boundaries (strip silences at the beginning and at the end of the segment).")
parser.add_argument("--recover-text-format", action="store_true", default=False, help="Recover text format and punctuation; requires --ind-file and --vocab-file options.")
parser.add_argument("--ind-file", help="IND file from the text preprocessing step.")
parser.add_argument("--vocab-file", help="VOCAB file from the text preprocessing step.")
parser.add_argument("--for-training", dest="train", action="store_true", default=False, help="Apply speech data filtering heuristics. Use --am-scores option to provide word alignment scores.")
parser.add_argument("--am-scores", dest="scores", default=None, help="Word alignment scores file.")
parser.add_argument("--min-segment-length", type=float, default=0.5)
parser.add_argument("--hlength-disable", dest="h_length", action="store_false")
parser.add_argument("--hlength-stdevs", type=float, default=2)
parser.add_argument("--hlength-max", type=float, default=1)
parser.add_argument("--hscore-disable", dest="h_score", action="store_false")
parser.add_argument("--hscore-factor", type=float, default=0.25)
parser.add_argument("--hscore-min", type=float, default=0)

args = parser.parse_args()

alignf = args.align
out_prefix = args.out_prefix


align = TLKAlign(silence_token="SP")
with open(alignf) as fd:
    align.read(fd)

scoresf = args.scores
if scoresf != None:
    with open(scoresf) as fd:
        align.import_scores(fd)

if args.generate_files:
        words_file = "%s.words.lst" % out_prefix
        with open(words_file, 'w') as fd:
            for w in align.words:
                fd.write("%s\n" % w)

if args.recover_text_format:
    if args.ind_file == None or args.vocab_file == None:
        parser.error("--recover-text-format requires --ind-file and --vocab-file options.") 
    fdind = open(args.ind_file)
    fdvoc = open(args.vocab_file)
    align.import_punctuation(fdind, fdvoc)
    if args.generate_files:
        words_punct_file = "%s.words.punct.lst" % out_prefix
        with open(words_punct_file, 'w') as fd:
            for w in align.words:
                fd.write("%s\n" % w)
    #if args.soft_boundaries:
    #     align.drop_context()
    #    if args.generate_files:
    #        words_punct_wo_context_file = "%s.words.punct.wo-context.lst" % out_prefix
    #        with open(words_punct_wo_context_file, 'w') as fd:
    #            for w in align.words:
    #                fd.write("%s\n" % w)


segment = TLKSegment()
segment.align = align


if args.keep_segmentation:
    segment.set_align_segments(soft_boundaries=args.soft_boundaries)
else:
    if args.train:
        segment.build_training_segments(sil_thold=args.sil_threshold, min_seg_len=args.min_segment_length, max_seg_len=args.max_segment_length,
                                h_length=args.h_length, h_length_stdevs=args.hlength_stdevs, h_length_max=args.hlength_max, 
                                h_score=args.h_score, h_score_factor=args.hscore_factor, h_score_min=args.hscore_min, debug=args.debug)
    else:
        segment.build_segments()

if args.generate_files:
    dfxp = segment.to_dfxp()
    update_status(dfxp)
    trg_dfxp = "%s.dfxp" % out_prefix
    with open(trg_dfxp, 'w') as fd:
        fd.write(dfxp.toXml())
   
    if args.recover_text_format:
        srt = segment.to_srt(word_spacing=False)
    else: 
        srt = segment.to_srt()
    trg_srt = "%s.srt" % out_prefix
    with open(trg_srt, 'w') as fd:
        fd.write(srt)
    
    trg_json = "%s.json" % out_prefix
    with open(trg_json, 'w') as fd:
        fd.write(segment.to_json_string())

if args.stats_file:
    if not(args.train):
        sys.stderr.write("[WW] Will not generate segmentation stats file, as we're keeping the original segmentation\n")
    else:
        with open(args.stats_file, 'w') as fd:
            fd.write(json.dumps(segment.stats, indent=4, sort_keys=True))
else:
    print json.dumps(segment.stats, indent=4, sort_keys=True)

