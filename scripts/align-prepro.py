import sys
import re
import unicodedata
import os

NON_SPEECH_TAGS = re.compile(r"music|noise|click|laugh|breath|ring|no-speech|overlap|lipsmack|cough|dtmf|sta|cry|prompt|applause|silence|nospeech")

class Processor:

    def __init__(self, exceptions, parse_mtp_tags=False):
        self._exceptions = exceptions
        self._i = 1
        self._li = [None]
        self._dw = {}
        self._text_ind = ""
        self._text_clean = ""
        self._st_in = False
        self._parse_mtp_tags = parse_mtp_tags
 
    def __get_ind(self, t, convert):
        if t not in self._dw:
            self._li.append((t, convert))
            self._dw[t] = self._i
            self._i += 1
        return self._dw[t]

    def __process_token(self, s):
        x = ""
        for c in s:
            cat = unicodedata.category(c)
            if cat[0] in ("L", "N", "M") or c in self._exceptions:
                x += c
            else:
                if x != "":
                    ind = self.__get_ind(x, not(self._st_in))
                    self._text_ind += "{%d}" % (ind)
                    self._text_clean += "%s " % (x)
                self._text_ind += c
                x = ""
        if x != "":
            ind = self.__get_ind(x, not(self._st_in))
            self._text_ind += "{%d}" % (ind)
            self._text_clean += "%s " % (x)

    def process_line(self, t):
        st_in = False
        for w in t.split():
            t = re.search(r"<[^>]+>|\([^)]+\)|\[[^]]+\]", w) # search for special tags e.g. [noise], <b>, </b>, (laugh).
            d0 = re.search(r"\d+[.,]\d+", w) # decimal numbers
            d1 = re.search(r"\d{1,3}(?:,\d{3})+(?:\.\d+)?", w) # numbers (american)
            d2 = re.search(r"\d{1,3}(?:\.\d{3})+(?:,\d+)?", w) # numbers (spain)
            if t != None:
                i = 0
                for x in re.finditer("<[^>]+>|\([^)]+\)|\[[^]]+\]", w):
                    pre = w[i:x.start(0)]
                    if len(pre) > 0:
                        self.__process_token(pre)
                    tag = x.group(0)
                    if self._parse_mtp_tags: # special tag
                        #if tag[1:6] == "lang:":
                        #    self._st_in = True
                        #    self._text_ind += "%s" % (tag)
                        #elif tag[1:7] == "/lang:":
                        #    self._st_in = False
                        #    self._text_ind += "%s" % (tag)
                        #elif NON_SPEECH_TAGS.search(tag):
                        #if NON_SPEECH_TAGS.search(tag):
                        ind = self.__get_ind(tag, False)
                        self._text_ind += "{%d}" % (ind)
                        self._text_clean += "%s " % (tag)
                        #else:
                        #    self._text_ind += "%s" % (tag)
                    else:
                        self._text_ind += "%s" % (tag)
                    i = x.end(0)
                post = w[i:]
                if len(post) > 0:
                    self.__process_token(post)
                    
            elif d0 != None or d1 != None or d2 != None: # number
                if d1 != None and d2 != None:
                    ld1 = d1.span(0)[1] - d1.span(0)[0]
                    ld2 = d2.span(0)[1] - d2.span(0)[0]
                    if ld1 >= ld2:
                        d = d1
                    else:
                        d = d2
                elif d1 != None or d2 != None:
                    d = d1 if d1 != None else d2
                else:
                    d = d0
                num = d.group(0)
                pre = w[:d.start(0)]
                post = w[d.end(0):]
                if len(pre) > 0:
                    self.__process_token(pre)

                ind = self.__get_ind(num, not(self._st_in))
                self._text_ind += "{%d}" % (ind)
                self._text_clean += "%s " % (num)
                
                if len(post) > 0:
                    self.__process_token(post)
           
            else: #regular text
                self.__process_token(w) 

            self._text_ind += " "
        self._text_ind += "\n"
        self._text_clean += "\n" 


if __name__ == "__main__":

    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("transcription", help="Transcription file")
    parser.add_argument("out_dir", help="Output directory to write files")
    parser.add_argument("-m", "--mtp-tags", action="store_true", default=False, help="Parse MTP special tags (i.e. [music], [lang:foreign] bla bla [/lang:foreign])")
    args = parser.parse_args()

 
    txtf = args.transcription
    out_dir = args.out_dir
    
    #exceptions = ["-", "·"]
    exceptions = []
    p = Processor(exceptions, parse_mtp_tags=args.mtp_tags) 

    with open(txtf) as fd:
        for l in fd:
            p.process_line(l)

    if not(os.path.exists(out_dir)):
        os.makedirs(out_dir)

    outind = "%s/transcript.ind" % out_dir
    outtxt = "%s/transcript.align.txt" % out_dir
    outvocab = "%s/transcript.vocab" % out_dir

    with open(outind, 'w', encoding='utf-8') as fd:
        fd.write(p._text_ind)

    with open(outtxt, 'w', encoding='utf-8') as fd:
        fd.write(p._text_clean)

    with open(outvocab, 'w', encoding='utf-8') as fd:
        for i in range(1, len(p._li)):
            fd.write("{%d} %s %d\n" % (i, p._li[i][0], int(p._li[i][1])))
