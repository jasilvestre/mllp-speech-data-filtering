set -eux
t=$1; vid=$2; 
destdir=debug-samples/de/$t.$vid
mkdir -p $destdir
python rebuild_filtered_alignments2.py --min-segment-length 0 --max-segment-length 5 --debug --sil-threshold 0.5 /home/jsilvestre/wrk/2018-10-15_align_de_data/$t/wrk/v1/$vid/align.tlk /home/jsilvestre/wrk/2018-10-15_align_de_data/$t/wrk/v1/$vid/align.scores.txt $destdir/$t.$vid > $destdir/$t.$vid.stats.json
auf=$(grep $vid /home/jsilvestre/wrk/2018-10-15_align_de_data/$t/objects.lst | awk -F ';' '{print $1}')
#ext=$(basename $auf)
#ext=${ext#*.}
#cp $auf $destdir/$t.$vid.$ext
ffmpeg -i $auf -acodec libmp3lame -b:a 128k $destdir/$t.$vid/$t.$vid.mp3 
cp /home/jsilvestre/wrk/2018-10-15_align_de_data/$t/wrk/v1/$vid/align.tlk $destdir/$t.$vid.tlkalign
cp /home/jsilvestre/wrk/2018-10-15_align_de_data/$t/wrk/v1/$vid/align.scores.txt $destdir/$t.$vid.tlkscores
rm $destdir/$t.$vid.dfxp
rm $destdir/$t.$vid.json
#echo "vlc $t.$vid.$ext --sub-file $t.$vid.srt --audio-visual visual" > $destdir/play.sh
echo "vlc $t.$vid.mp3 --sub-file $t.$vid.srt --audio-visual visual" > $destdir/play.sh
chmod +x $destdir/play.sh
