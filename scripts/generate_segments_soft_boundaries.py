#!/usr/bin/python3

import sys
import os
import argparse
import re


RE_SPECIAL_TAGS = re.compile(r"<[^>]+>|\([^)]+\)|\[[^]]+\]")
CTXT_START_TOK = ">>>>>||"
CTXT_END_TOK = "||<<<<<"

if __name__ == "__main__":


    parser = argparse.ArgumentParser()
    parser.add_argument("segments", help="Segments file")
    parser.add_argument("margin", type=float, help="Margin duration in seconds")
    args = parser.parse_args()

    segf = args.segments
    margin = abs(args.margin)
    
    segs = []
    with open(segf) as fd:
        for l in fd:
            l = l.strip()
            m = re.search(r"([0-9.]+) ([0-9.]+) (.*)", l)
            st = float(m.group(1))
            et = float(m.group(2))
            txt = m.group(3)
            if st > et:
                sys.stderr.write("[WW] Inconsistent segment timestamps: %.3f %.3f %s\n" % (st, et, txt))
            else:
                segs.append((st, et, txt))

    segs = sorted(segs)

    i = 0
    while i < len(segs):
        seg = segs[i]
        #print("--", seg[0], seg[1], seg[2])
        # Define the new segment start time and prepend words from previous overlapped segments
        st = max(seg[0] - margin, 0)
        pre_text = ""
        j = i - 1
        overlap = True
        # look for previous overlapping segments
        while j >= 0 and overlap:
            prev_seg = segs[j]
            prev_seg_st = max(prev_seg[0] - margin, 0)
            prev_seg_et = prev_seg[1] + margin
            prev_seg_text = prev_seg[2]
            if st < prev_seg_et: # exists overlapping, get proportional words from the segment and preppend
                prev_seg_dur = prev_seg_et - prev_seg_st
                #prev_seg_dur = prev_seg[1] - prev_seg[0]
                overlap_dur = prev_seg_et - st
                overlap_ratio = overlap_dur / float(prev_seg_dur)
                if overlap_ratio < 1:
                    overlap = False
                prev_seg_text_len = max(len(RE_SPECIAL_TAGS.sub("", prev_seg_text).strip()), 1) # do not account special tags 
                overlap_chars = max(round(overlap_ratio * prev_seg_text_len), 1) # at least 1 char at minimum of overlapping
                #print("B", prev_seg_st, prev_seg_et, st, overlap_dur, overlap_ratio, overlap_chars)
                prev_seg_text_l = prev_seg_text.split()
                k = len(prev_seg_text_l) - 1
                c = 0
                while c < overlap_chars and k >= 0: 
                    w = prev_seg_text_l[k]
                    if RE_SPECIAL_TAGS.search(w) == None: 
                        # only count characters if the word is not a special tag e.g. [noise], <b>, </b>, (laugh).
                        c += (len(w)+2) # +2 -> include spaces
                    k -= 1
                t = " ".join(prev_seg_text_l[k+1:])
                pre_text = "%s %s" %(t, pre_text)
            else:
                overlap = False
            j -= 1
        # Define the new segment end time and append words from next overlapped segments
        et = seg[1] + margin
        if et < st: # Fix for bad timestamps in origin
            et = seg[0] + margin
        post_text = ""
        j = i + 1
        overlap = True
        # look for next overlapping segments
        while j < len(segs) and overlap:
            next_seg = segs[j]
            next_seg_st = max(next_seg[0] - margin, 0)
            next_seg_et = next_seg[1] + margin
            next_seg_text = next_seg[2]
            if et > next_seg_st: # exists overlapping, get proportional words from the segment and append
                next_seg_dur = next_seg_et - next_seg_st
                #next_seg_dur = next_seg[1] - next_seg[0]
                overlap_dur = et - next_seg_st
                overlap_ratio = overlap_dur / float(next_seg_dur)
                if overlap_ratio < 1:
                    overlap = False
                next_seg_text_len = max(len(RE_SPECIAL_TAGS.sub("", next_seg_text).strip()), 1) # do not account special tags 
                overlap_chars = max(round(overlap_ratio * next_seg_text_len), 1) # at least 1 char at minimum of overlapping
                #print("A", next_seg_st, next_seg_et, et, overlap_dur, overlap_ratio, overlap_chars)
                next_seg_text_l = next_seg_text.split()
                k = 0
                c = 0
                while c < overlap_chars and k < len(next_seg_text_l): 
                    w = next_seg_text_l[k]
                    if RE_SPECIAL_TAGS.search(w) == None: 
                        # only count characters if the word is not a special tag e.g. [noise], <b>, </b>, (laugh).
                        c += (len(w)+2) # +2 -> include spaces
                    k += 1
                t = " ".join(next_seg_text_l[:k])
                post_text = "%s %s" %(post_text, t)
            else:
                overlap = False
            j += 1


        text = "%s %s %s %s %s" % (pre_text, CTXT_START_TOK, seg[2], CTXT_END_TOK, post_text)
        print("%.3f %.3f %s" % (st, et, text))

        i += 1
       
