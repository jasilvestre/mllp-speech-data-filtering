#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import json
import cStringIO
import sys
import json
from math import log, exp, sqrt
from datetime import datetime
from TLKAlign import *
from libdfxp import *

class Segment:
    def __init__(self, words):
        if (len(words) > 0):
            self.begin = words[0].begin
            self.end = words[-1].end
        else:
            self.begin = 0
            self.end = 0
        self.words = words

    def __repr__(self):
        return '<%.2f,%.2f,%s>'%(self.begin, self.end, self.get_text())

    def add_word(self, word):
        if (len(self.words) == 0): self.begin = word.begin
        self.end = word.end
        self.words.append(word)

    def get_num_words(self):
        return len(self.words)

    def get_text(self):
        return ' '.join(w.word.encode('utf-8') for w in self.words)

    def get_length(self):
        return len(self.get_text())

    def get_duration(self):
        return self.end-self.begin

    def get_character_rate(self):
        if (len(self.words) == 0): return 0
        return self.get_length()/float(self.get_duration())

    def get_confidence(self):
        conf_log_sum = 0
        if len(self.words) == 0:
            return None
        for w in self.words:
            if w.conf is not None:
                conf_log_sum += log(w.conf)
            else:
                return None
        return exp(conf_log_sum/float(len(self.words)))

    def to_json_string(self):
        ret = {}
        if (len(self.words) == 0): return ret
        ret['b'] = float(self.begin)
        ret['e'] = float(self.end)
        ret['wl'] = []
        for w in self.words:
            ret['wl'].append({"b":w.begin,"e":w.end,"w":w.word})
        return ret

    def to_TlSegment(self, dfxp, segmentId):
        tlSeg = TlSegment(dfxp, str(segmentId), self.begin, self.end)
        tlSeg.confMeasure = self.get_confidence()
        tlSeg.data = []
        for w in self.words:
            word = TlWord(dfxp)
            word.begin = w.begin
            word.end = w.end
            word.txt = w.word
            word.confMeasure = w.conf
            tlSeg.data.append(word)
        return tlSeg

class TLKSegment:

    def __init__(self):
        self.segments = []
        self.align = None
        
    def import_TLK_align(self, f):
        fd = open(f)
        self.align = TLKAlign()
        self.align.read(fd)

    def import_TLK_align_str(self, s):
        self.align = TLKAlign()
        self.align.read_str(s)

    def set_align_segments(self):
        for sample in self.align.samples_words:
            seg = Segment(sample.words) 
            seg.begin = sample.begin
            seg.end = sample.end
            self.segments.append(seg)

    def build_segments(self, sil_thold=2, s_max_chars=74, s_opt_chars=48, s_max_chrate=19):
        self.segments = []
        if (len(self.align.words) == 0): return

        # 1 - Divide words into "big segments" according to sil_thold
        last_i = 0
        for i in range(len(self.align.words)-1):
            sep = self.align.words[i+1].begin-self.align.words[i].end
            if (sep > sil_thold):
                self.segments.append(Segment(self.align.words[last_i:i+1]))
                last_i = i+1
        if last_i < len(self.align.words):
            self.segments.append(Segment(self.align.words[last_i:len(self.align.words)]))

        # 2 - Calculate average and std. deviation silence duration between words
        avg_sil_dur = 0
        stdev_sil_dur = 0
        num_words = 0
        for i in range(len(self.segments)):
            if len(self.segments[i].words) > 0:
                num_words += len(self.segments[i].words)-1
                for j in range(len(self.segments[i].words)-1):
                    avg_sil_dur += (self.segments[i].words[j+1].begin-self.segments[i].words[j].end)
        if num_words > 0:
            avg_sil_dur = avg_sil_dur/float(num_words)
            stdev_sil_dur = 0
            for i in range(len(self.segments)):
                for j in range(len(self.segments[i].words)-1):
                    stdev_sil_dur += pow((self.segments[i].words[j+1].begin-self.segments[i].words[j].end)-avg_sil_dur, 2)
            stdev_sil_dur = sqrt(stdev_sil_dur)/float(num_words)

        # 3 - Divide "big segments" into smaller segments according to avg_sil_thold
        avg_sil_thold = (2*avg_sil_dur)+(2*stdev_sil_dur)
        s_segments = []
        for bs in self.segments:
            seg = Segment([])
            for wi in range(len(bs.words)):
                seg.add_word(bs.words[wi])
                if (wi == len(bs.words)-1):
                    s_segments.append(seg)
                else:
                    if (seg.get_length() > s_opt_chars*0.8):
                        ### Segment text length near optimum value, consider splitting
                        if (bs.words[wi+1].begin-seg.end > avg_sil_thold):
                            s_segments.append(seg)
                            seg = Segment([])
                        elif (seg.get_length() > s_opt_chars*1.2 or seg.get_length() > s_max_chars*0.9):
                            s_segments.append(seg)
                            seg = Segment([])
        self.segments = s_segments

        # 4 - Merge short segments with the closest nearby segment
        short_max_length = 12
        s_merged_segments = [self.segments[0]]
        i = 1
        while i < len(self.segments):
            if self.segments[i].get_length() < short_max_length:
                prev_dist = self.segments[i].begin-self.segments[i-1].end
                prev_length = self.segments[i-1].get_length()+self.segments[i].get_length()
                next_dist = self.segments[i+1].begin-self.segments[i].end if i < len(self.segments)-1 else sys.maxint
                next_length = self.segments[i+1].get_length()+self.segments[i].get_length() if i < len(self.segments)-1 else sys.maxint
                if (prev_dist <= next_dist and prev_dist < sil_thold and prev_length <= s_max_chars):
                    s_merged_segments.pop()
                    m_seg = self.segments[i-1]
                    for w in self.segments[i].words:
                        m_seg.add_word(w)
                    s_merged_segments.append(m_seg)
                elif (next_dist < sil_thold and next_length <= s_max_chars):
                    m_seg = self.segments[i]
                    for w in self.segments[i+1].words:
                        m_seg.add_word(w)
                    s_merged_segments.append(m_seg)
                    i += 1
                else:
                    s_merged_segments.append(self.segments[i])
                    
            else: s_merged_segments.append(self.segments[i])
            i += 1
        self.segments = s_merged_segments

        # 5 - Extend segments faster than s_max_chrate (DEPRECATED)

    def build_training_segments(self, sil_thold=0.05, min_seg_len=0.5, max_seg_len=20, 
                                      h_length=True, h_length_stdevs=2, h_length_max=1, 
                                      h_score=True, h_score_factor=0.25, h_score_min=0):
        h_length_max *= 100 # seconds to frames
        self.segments = []
        if (len(self.align.words) == 0): return

        self.stats = { "score_total_sum":0, 
                       "score_mean":0,
                       "score_stdev":0,
                       "pho_len_mean":0,
                       "pho_len_stdev":0,
                       "length_raw":0,
                       "length_aligned":0,
                       "length_after_cleaning":0,
                       "words_aligned":0,
                       "words_after_cleaning":0,
                       "segments_after_cleaning":0,
                       "dropped_words_by_score":{"n":0, "dur":0},
                       "dropped_words_by_length":{"n":0, "dur":0},
                       "dropped_segments_by_length":{"n":0, "dur":0},
                       "acceptance_ratio":None
                      }

        silence_token = self.align.sil

        self.stats["words_aligned"] = len(self.align.words)

        # Mean and std phoneme lengths
        n = 0; s = 0; s2 = 0; t = 0
        for k in self.align.samples.keys():
            sample = self.align.samples[k]
            for e in sample:
                if e.phoneme != silence_token:
                    s += e.duration 
                    s2 += e.duration * e.duration
                    n += 1
                t += e.duration
        if n == 0:
            return

        m_pho = s/float(n)
        var_pho = (s2/float(n)) - (m_pho*m_pho)
        std_pho = sqrt(var_pho)
        max_pho_len_thold = min( int( round(m_pho + h_length_stdevs*std_pho) ), h_length_max)

        self.stats["length_raw"] = t/100.0
        self.stats["length_aligned"] = s/100.0
        self.stats["pho_len_mean"] = m_pho
        self.stats["pho_len_stdev"] = std_pho
        self.stats["pho_len_threshold"] = max_pho_len_thold

        use_scores = False
        if self.align.words[0].score != None and h_score:
            use_scores = True

        if use_scores:
            # FIXME Bug "wgraph.py -A" ? Word frame occupations differ between wordgraph and align 
            # Mean and std score per frame
            n = 0; s = 0; s2 = 0
            for w in self.align.words:
                nfs = w.score / float(w.score_len) # Normalised frame score
                s += nfs
                s2 += nfs*nfs 
                n += 1 
            m_sco = s/float(n)
            var_sco = (s2/float(n)) - (m_sco*m_sco)
            std_sco = sqrt(var_sco)
            min_sco_thold = max(m_sco * h_score_factor, h_score_min)
        
            self.stats["score_total_sum"] = s
            self.stats["score_mean"] = m_sco
            self.stats["score_stdev"] = std_sco
            self.stats["score_threshold"] = min_sco_thold
        else:
            self.stats["score_total_sum"] = None
            self.stats["score_mean"] = None
            self.stats["score_stdev"] = None
            self.stats["score_threshold"] = None
            

        # Drop words with high frame-consuming phonemes or below the alignment score threshold
        filt_words = []
        for i in xrange(len(self.align.words)):
            w = self.align.words[i]
            wlen = (w.end - w.begin) * 100
            drop = False
            if wlen == 0: # Fix for forced alignments (words mapped to SP)
                drop = True
            if not(drop) and h_length: # delete words with a high mean phoneme length 
                fpp = wlen / float(len(w.entries)) 
                if fpp > max_pho_len_thold:
                    self.stats["dropped_words_by_length"]["n"] += 1
                    self.stats["dropped_words_by_length"]["dur"] += (w.end-w.begin)
                    drop = True
            if not(drop) and h_length: # delete words containing a long phoneme
                for e in w.entries:
                    if e.duration > max_pho_len_thold:
                        self.stats["dropped_words_by_length"]["n"] += 1
                        self.stats["dropped_words_by_length"]["dur"] += (w.end-w.begin)
                        drop = True
                        break
            if not(drop) and use_scores: # delete words with a low normalised frame score
                spf = w.score/float(w.score_len)
                if spf < min_sco_thold:
                    drop = True
                    self.stats["dropped_words_by_score"]["n"] += 1
                    self.stats["dropped_words_by_score"]["dur"] += (w.end-w.begin)
            if not(drop):
                filt_words.append(w)
        
        self.align.words = filt_words

        last_i = 0
        for i in range(len(self.align.words)-1):
            sep = self.align.words[i+1].begin-self.align.words[i].end
            # Join words separated less that sil_thold seconds into big segments.
            if (sep > sil_thold):
                add = True
                seg_len = self.align.words[i].end - self.align.words[last_i].begin
                if seg_len < min_seg_len: # drop short segments
                    add = False
                    self.stats["dropped_segments_by_length"]["n"] += 1
                    self.stats["dropped_segments_by_length"]["dur"] += seg_len
                if add:
                    wlist = self.align.words[last_i:i+1]
                    self.stats["words_after_cleaning"] += len(wlist)
                    if seg_len > max_seg_len:
                        ret = self.split_segments(wlist, max_seg_len)
                        for r in ret:
                            self.segments.append(Segment(r))
                            self.stats["segments_after_cleaning"] += 1
                    else:
                        self.segments.append(Segment(wlist))
                        self.stats["segments_after_cleaning"] += 1
                    self.stats["length_after_cleaning"] += seg_len
                last_i = i+1
        if last_i < len(self.align.words):
            add = True
            seg_len = self.align.words[len(self.align.words)-1].end - self.align.words[last_i].begin
            if seg_len < min_seg_len:
                self.stats["dropped_segments_by_length"]["n"] += 1
                self.stats["dropped_segments_by_length"]["dur"] += seg_len
                add = False
            if add:
                wlist = self.align.words[last_i:len(self.align.words)]
                self.stats["words_after_cleaning"] += len(wlist)
                if seg_len > max_seg_len:
                    ret = self.split_segments(wlist, max_seg_len)
                    for r in ret:
                        self.segments.append(Segment(r))
                        self.stats["segments_after_cleaning"] += 1
                else:
                    self.segments.append(Segment(wlist))
                    self.stats["segments_after_cleaning"] += 1
                self.stats["length_after_cleaning"] += seg_len
        # "length_after_cleaning" can be bigger than "length_aligned", given that the first one accounts segments (including silences between words),
        self.stats["acceptance_ratio"] = min(self.stats["length_after_cleaning"] / self.stats["length_aligned"] * 100, 100)
 
    def split_segments(self, wlist, max_seg_len):
        ret = []
        ini = 0; j = 1
        sbeg = wlist[0].begin
        while j < len(wlist):
            if wlist[j].end - sbeg > max_seg_len:
                ret.append(wlist[ini:j])
                sbeg = wlist[j].begin
                ini = j
            j += 1
        ret.append(wlist[ini:j])
        return ret
             

    def print_segments(self):
        for i in range(len(self.segments)):
            print self.segments[i]

    def to_json_string(self):
        ret = []
        for s in self.segments:
            ret.append(s.to_json_string())
        return json.dumps(ret)

    def to_dfxp(self, video_id=None, author_id=None, author_conf=None, author_type=None, time_stamp=datetime.now()):
        dfxp = TlDfxp()
        dfxp.videoId = video_id
        dfxp.authorId = author_id
        dfxp.authorConf = author_conf
        dfxp.authorType = author_type
        dfxp.timeStamp = time_stamp
        dfxp.dfxp = dfxp
        segId = 1
        for s in self.segments:
            dfxp.data.append(s.to_TlSegment(dfxp, segId))
            dfxp.oridata.append(s.to_TlSegment(dfxp, segId))
            segId += 1
        # Calculate global confidence measure
        if len(self.segments) == 0:
            dfxp.confMeasure = None
        else:
            conf_log_sum = 0
            for s in self.segments:
                seg_conf = s.get_confidence()
                if seg_conf is not None:
                    conf_log_sum += log(seg_conf)
                else:
                    conf_log_sum = None
                    dfxp.confMeasure = None
                    break
            if (conf_log_sum is not None):
                dfxp.confMeasure = exp(conf_log_sum/float(len(self.segments)))
        return dfxp
        
   
if __name__ == "__main__":
    segment = TLKSegment()
    segment.import_TLK_align(sys.argv[1])
    segment.build_segments()
    # Print DFXP
    dfxp = segment.to_dfxp()
    print dfxp.toXml()
    # Print JSON
    #### print segmenter.to_json_string()
