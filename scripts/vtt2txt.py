import webvtt
import sys

vtt = sys.argv[1]

vtt = webvtt.read(vtt)
transcript = ""

lines = []
for line in vtt:
    lines.extend(line.text.strip().splitlines())

previous = None
for line in lines:
    line = line.strip()
    if line == "": continue
    if line == previous: continue
    print(line)
    previous = line

