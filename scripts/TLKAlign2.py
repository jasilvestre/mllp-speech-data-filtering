#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import json
import cStringIO
import unicodedata
import re

NON_SPEECH_TAGS = re.compile(r"music|noise|click|laugh|breath|ring|no-speech|overlap|lipsmack|cough|dtmf|sta|cry|prompt|applause")
PUNCT_JOIN_RIGHT = re.compile(r"¿|¡|'")
CTXT_START = re.compile(r">>>>>\|\|(.*)")
CTXT_END = re.compile(r"(.*)\|\|<<<<<")

class AlignEntry:

    def __init__(self, begin, duration, word, phoneme=None, phoneme_pos=None):
        self.begin = begin
        self.duration = duration
        self.word = word
        self.phoneme = phoneme
        self.pos = phoneme_pos

    def __repr__(self):
        return '<%d,%d,%s,%d,%s>'%(self.begin, self.duration, self.phoneme, self.pos, self.word.encode('utf-8'))

class Word:

    def __init__(self, begin, end, word, entries, conf=None):
        self.begin = begin 
        self.end = end
        self.word = word
        self.conf = conf
        self.entries = entries
        self.score = None
        self.score_len = None
        self.drop = False
        self.drop_by = None

    def __repr__(self):
        return '<%.2f,%.2f,%s>'%(self.begin,self.end, self.word.encode('utf-8'))

    def __iter__(self):
        yield 'b', self.begin
        yield 'e', self.end
        yield 'w', self.word
        yield 'c', self.conf

class Sample:

    def __init__(self, begin, end, words) :
        self.begin = begin
        self.end = end
        self.words = words

    def __repr__(self):
        return '<%.2f,%.2f,%s>'%(self.begin,self.end, str(self.words))

    def __iter__(self):
        yield 'b', self.begin
        yield 'e', self.end
        yield 'wl', self.words


class TLKAlign:

    WORD_ALIGN = "WORD"
    SYM_ALIGN = "SYM"

    def __init__(self, align_type=SYM_ALIGN, silence_token="SP"):
        self.type = align_type
        self.sil = silence_token
        self.samples = {}
        self.sample_names_list = []
        self.words = []
        self.samples_words = []
        
    def read(self, fd):
        name, entries = self.__read_sample(fd)
        while entries != None:
            self.sample_names_list.append(name)
            self.samples[name] = entries
            name, entries = self.__read_sample(fd)
        self.__create_word_list()

    def read_str(self, string):
        fd = cStringIO.StringIO(string)
        self.read(fd)

    def export_wordlist_json(self):
        l = [ dict(w) for w in self.words ]
        return json.dumps(l, indent=4, encoding='UTF-8')

    def export_samples_wordlist_json(self):
        l = [ dict(s) for s in self.samples_words ]
        return json.dumps(l, indent=4, encoding='UTF-8')

    def import_cms(self, fd):
        if self.words == []:
            return
            #raise Exception("You must load first a TLK Align file.")
        ret = self.__read_cm(fd)
        if len(ret) != len(self.words):
            for i in xrange(len(self.words)):
                print i, self.words[i].word, ret[i][0]
            raise Exception("Number of words of TLK Align File and CM file differs (%d != %d)." % (len(self.words), len(ret)))
        for i in xrange(len(self.words)):
            if self.words[i].word != ret[i][0]:
                raise Exception("Non-matching %d-th words: [align] %s -> [cm] %s\n"%(i, self.words[i].word.encode('utf-8'), ret[i][0].encode('utf-8')))
            self.words[i].conf = ret[i][1]

    def import_scores(self, fd):
        if self.words == []:
            return
            #raise Exception("You must load first a TLK Align file.")
        ret = self.__read_scores(fd)
        if len(ret) != len(self.words):
            import sys
            for i in xrange(min(len(self.words), len(ret))):
                sys.stderr.write("%d %s %s\n" %(i, self.words[i].word.encode('utf-8'), ret[i][0].encode('utf-8')))
            raise Exception("Number of words of TLK Align File and Score file differs (%d != %d)." % (len(self.words), len(ret)))
        for i in xrange(len(self.words)):
            if self.words[i].word != ret[i][0]:
                raise Exception("Non-matching %d-th words: [align] %s -> [score] %s\n"%(i, self.words[i].word.encode('utf-8'), ret[i][0].encode('utf-8')))
            self.words[i].score = ret[i][1]
            self.words[i].score_len = ret[i][2]

    def drop_context(self):
        wordl = []
        inside = False
        for i in xrange(len(self.words)):
            w = self.words[i].word
            mstart = CTXT_START.search(w)
            mend = CTXT_END.search(w)
            if mstart != None and mend != None: # i.e. >>>>>||Yes||<<<<< (should not happen!)
                txt = mstart.group(1)
                txt = CTXT_END.sub(r"\1", txt)
                if txt.strip() != "":
                    self.words[i].word = txt
                    wordl.append(self.words[i])
                inside = False
            elif mstart != None: # i.e. Bye! >>>>>||
                inside = True
                txt = mstart.group(1)
                if txt.strip() != "":  # i.e. >>>>>|| Hi!
                    self.words[i].word = txt
                    wordl.append(self.words[i])
            elif mend != None:
                txt = mend.group(1)
                if txt.strip() != "":
                    self.words[i].word = txt
                    wordl.append(self.words[i])
                inside = False
            else:
                if inside:
                    wordl.append(self.words[i])
        self.words = wordl
            

    def import_punctuation(self, fdind, fdvoc):
        txt_ind = " ".join(fdind.readlines()).decode('utf-8')
        voc = { x.split()[0].decode('utf-8'):x.split()[1].decode('utf-8') for x in fdvoc }
        nwords = len(re.findall(r"{\d+}", txt_ind))
        if nwords != len(self.words):
            raise Exception("IND file has %d words != Align has %d words" % (nwords, len(self.words)))
        i = 0 # index of alignment words
        w_align = self.words[i].word # current word from the align file
        j = 0 # index of characters from the ind file
        w_new = "" # "augmented" string: will become the actual word + all other characters around it.
        found = False # Describes whether the next aligned word has been found in the ind file or not.
        while j < len(txt_ind):
            c = txt_ind[j] # get j-th character
            cat = unicodedata.category(c) # get unicode category of the character c
            #print "%s|%s|%s|%s" % (c.encode('utf-8'), cat, w_new.encode('utf-8'), w_align.encode('utf-8'))
            if c == "{": 
                m = re.match(r"{\d+}", txt_ind[j:]) # check if we have found a word index in the ind string
                if m != None: # we have found a word index
                    if found: # we're still in the previous word and we've found another one; update align word with the punctuation before shifting to the next one. 
                        #print "\tAlign update: |%s|%s|" % (self.words[i].word.encode('utf-8'), w_new.encode('utf-8'))
                        self.words[i].word = w_new
                        i += 1
                        if i < len(self.words): 
                            w_align = self.words[i].word
                        w_new = ""
                        found = False
                    wid = txt_ind[j:j+m.end()] # get word index 
                    w = voc[wid] # get word given its index
                    #print "\t-> %s|%s" % (wid.encode('utf-8'), w.encode('utf-8'))
                    if w != w_align: # if the word we have found in the ind string doesn't match with the following align word
                       msg = "Next align word is %s, but I expect %s -> %s" % (w_align, wid, w)
                       raise Exception(msg.encode('utf-8'))
                    w_new += w # append the aligned word to the "augmented" word string
                    j += m.end() # shift j to the position after the word index token
                    found = True 
                else: # it's not a word index, just a {. Append it to the "augmented" word string
                    w_new += c
                    j += 1
            else: # regular character
                if cat[0] != "C": # Append to the "augmented" word string anything but control characters
                    w_new += c
                j += 1
                if found and cat[0] in ("Z", "C"): # If we already processed the current alignment word and we find a space or control character, update align word and shift to the next one.
                    #print "\tAlign update: |%s|%s|" % (self.words[i].word.encode('utf-8'), w_new.encode('utf-8'))

                    # Keep accumulating the following tokens until we reach an opening punctuation mark
                    cont = True
                    while cont and j < len(txt_ind):
                        c_next = txt_ind[j]
                        cat_next = unicodedata.category(c_next)
                        if PUNCT_JOIN_RIGHT.match(c_next) or cat_next in ("Ps", "Pi"): # if "¡¿" OR Ps=Punctuation_Open (like "(","[","{") OR Pi=Punctuation_Initial_Quote
                            cont = False
                        else:
                            w_new += c_next
                            j += 1

                    self.words[i].word = w_new
                    i += 1
                    if i < len(self.words): 
                        w_align = self.words[i].word
                    w_new = ""
                    found = False
                
        if i != len(self.words):
            raise Exception("Finished with IND file, but there are words remaining in the alignment! (%d < %d)" % (i, len(self.words)))

        if w_new != "":
            self.words[i-1].word += w_new


    def __read_cm(self,fd):
        ret = []
        for l in fd:
            fields = l.strip().split()
            w = fields[0].decode('utf-8')
            cm = float(fields[1])
            ret.append((w,cm))
        return ret

    def __read_scores(self,fd):
        ret = []
        for l in fd:
            fields = l.strip().split()
            w = fields[0].decode('utf-8')
            score = float(fields[1])
            wlen = int(fields[2])
            ret.append((w,score,wlen))
        return ret


    def __read_sample(self, f):
        # Descarta capçalera
        sample_name = ""
        l = f.readline()
        while l != '':
            if l[0] == '"':
                sample_name = os.path.basename(l.split('"')[1])
            line = l.strip()
            l = f.readline()
            if line != '' : break
        # Aliniaments
        entries = []
        while l != '' and l.strip()!='.':
            line = l.strip()
            l = f.readline()
            if line == '': continue
            if self.type == TLKAlign.SYM_ALIGN:
                b, d, p, pp, w = line.split()
            elif self.type == TLKAlign.WORD_ALIGN:
                b, d, w = line.split()
                p = None
                pp = 0
            else:
                raise Exception("Unknown align type.")
            entry = AlignEntry(int(b), int(d), w.decode('utf-8'), p, int(pp))
            entries.append(entry)
        if l == '' and len(entries) == 0:
            return None, None
        elif l == '' and len(entries) > 0:
            raise Exception("Bad TLK align file format: unexpected EOF.")
        else:
            return sample_name, entries

    def __create_word_list(self):
        #for name in sorted(self.samples.keys()):
        #for name in sorted(self.samples.keys(), key=lambda x: float(x.split("-")[0].split("_")[0])):
        for name in self.sample_names_list: #FIXME!! hotfix for --soft-boundaries
            # Extract sample global start and end times from sample name
            try:
                st = float(name.split("-")[0].split("_")[0])
                et = float(name.split("-")[0].split("_")[1])      
            except:
                raise Exception("Could not extract start and end times from sample name: '%s'" % name)
            words = self.__parse_sample(st, self.samples[name])
            self.words += words
            self.samples_words.append(Sample(st, et, words))
 
    def __is_nonspeech_word(self, i, entries):
        if entries[i].phoneme == self.sil:
            #if NON_SPEECH_TAGS.search(entries[i].word) == None:
            #    return False
            if i == 0:
                samew_prev_ent = False
            else:
                samew_prev_ent = (entries[i-1].word == entries[i].word)
            if i == len(entries) - 1:
                samew_next_ent = False
            else:
                samew_next_ent = (entries[i+1].word == entries[i].word)
            if not(samew_prev_ent) and not(samew_next_ent):
                return True
            else:
                return False
        else:
            return False
        

    def __parse_sample(self, gst, entries):
        if len(entries) == 0:
            return []
        words = []
        i = 0
        cbegin = 0
        wentries = []
        # First Entry
        entry = entries[i]
        if entry.pos != 0:
            cbegin += entry.duration
            i += 1
            entry = entries[i]
        if self.__is_nonspeech_word(i, entries):
            w = Word(gst + (entry.begin/100.0), gst + ((entry.begin + entry.duration)/100.0), entry.word, [entry])
            words.append(w)
            cword = None
            cdur = 0
        else:
            cword = entry.word
            if entry.phoneme == self.sil: # Word begins with a silence
                cbegin += entry.duration
                cdur = 0
            else:
                cdur = entry.duration
                wentries.append(entry)
        # Remaining entries
        i += 1
        while i < len(entries):
            entry = entries[i]
            if entry.pos > 0:
                if entry.phoneme == self.sil: # Silence at the end of the current word
                    # Add word
                    # IMPORTANT: To make robust forced-alignment processes, i'll not raise any exception.
                    #if cdur == 0:
                    #    raise Exception("Tried to add a 0-length word (Sample: %s, position %d)" % (name, entry.begin))
                    #print cword.encode('utf-8'), gst, cbegin,cbegin/100.0, gst + (cbegin/100.0)
                    w = Word(gst + (cbegin/100.0), gst + ((cbegin + cdur)/100.0), cword, wentries)
                    words.append(w)
                    # Update status
                    cword = None
                    cbegin = None
                    cdur = 0
                    wentries = []
                else:
                    cdur += entry.duration
                    wentries.append(entry)
            else:
                if cword != None: # Current word hasn't been added yet 
                    #if cdur == 0:
                    #    raise Exception("Tried to add a 0-length word (Sample: %s, position %d)" % (name, entry.begin))
                    w = Word(gst + (cbegin/100.0), gst + ((cbegin + cdur)/100.0), cword, wentries)
                    words.append(w)

                if self.__is_nonspeech_word(i, entries):
                    w = Word(gst + (entry.begin/100.0), gst + ((entry.begin + entry.duration)/100.0), entry.word, [entry])
                    words.append(w)
                    cword = None
                    cbegin = None
                    cdur = 0
                    wentries = []
                else:
                    cword = entry.word
                    wentries = []
                    if entry.phoneme == self.sil: # Word begins with a silence
                        cbegin = entry.begin + entry.duration # Word actually starts after silence
                        cdur = 0
                    else:
                        cbegin = entry.begin
                        cdur = entry.duration
                        wentries.append(entry)
            i += 1

        #if cdur > 0:
        if cword != None:
            w = Word(gst + (cbegin/100.0), gst + ((cbegin + cdur)/100.0), cword, wentries)
            words.append(w)
         
        return words
        

   
if __name__ == "__main__":

    import sys
 
    # Read file
    align = TLKAlign()
    fd = open(sys.argv[1])
    align.read(fd)
    fdind = open(sys.argv[2])
    fdvoc = open(sys.argv[3])
    align.import_punctuation(fdind, fdvoc)
    for w in align.words:
        print w.begin, w.end, w.word.encode('utf-8') 
    align.drop_context()
    print "\n\n---------------------------\n\n"
    for w in align.words:
        print w.begin, w.end, w.word.encode('utf-8') 

