# MLLP speech data filtering tools

We have two tools for speech data filtering.

- [`align.sh`](#alignsh): force-align a video/audio file against its transcription, and optionally apply filtering heuristics to generate training segments.
- [`asr-wer-rank.sh`](#asr-wer-ranksh): apply ASR to a list of audio/video samples and rank them by (normalized) wer/cer scores.

## align.sh

Force-align a video/audio file against its transcription, and optionally apply filtering heuristics to generate training segments.

### Requirements

**tlk >= 1.12.1**

### Usage

```
./align.sh <CONFIG_FILE> <IN_MEDIA> <SEGMENTS_LIST> <OUT_DIR> -b <TLK_BIN_DIR> -t <TMP_DIR> -T
```
 
Where:  

- `<CONFIG_FILE>`: alignment system configuration variables, i.e. acoustic model files, phonetic transcriptor, prune options, etc.
- `<IN_MEDIA>`: video or audio file to be aligned.
- `<SEGMENTS_LIST>`: a list of segments to be aligned, one per line, where the first token is the start time (in seconds), the second is the end time, and from the third token until the end of the line, the transcription text to be aligned within the given start and end times. Example:
```
0 3.412 Hello everybody!
5.021 9.153 We're are going to start with the MLLP's alignment tool.
```
When filtering crawled data, we typically align all the text extracted from the subtitles against the whole audio, so in this file we put only one line (segment).

- `<OUT_DIR>`: output directory where the results of the alignment will be placed.
- `<TLK_BIN_DIR>`: TLK's build bin directory, as the tool uses our ASR toolkit to perform the alignment.
- `<TMP_DIR>`: Where to store tmp files.
- `-T` option: tells the script to apply heuristics based on phoneme length ratios and alignment scores to filter out words with bad alignments, generating "filtered" versions of the alignment.

The idea is to submit this job to SGE. In general, it's enough to request 5GB of RAM and 2GB of GPU memory (in case it is possible to scale single GPUs). 
This will generate the following files inside the output directory:
 
- `align.dfxp` -> Forced-aligned segments (subtitles, in DFXP format)
- `align.srt` -> Forced-aligned segments (subtitles, in SRT format)
- `align.json` -> Forced-aligned segments (subtitles, in JSON format)
- `align.scores.txt` -> Acoustic scores and frame lengths at word level. 
- `align.filtered.dfxp` -> Forced-aligned and heuristically-filtered segments (filtered subtitles, in DFXP format)
- `align.filtered.srt` Forced-aligned and heuristically-filtered segments (filtered subtitles, in SRT format)
- `align.filtered.json` -> Forced-aligned and heuristically-filtered segments (filtered subtitles, in JSON format)
- `align.tlk` -> Raw alignment output from tLrecognise (TLK).

For training purposes, we'll pick the align.filtered.xxx files. 

 
At the end of the align.log file, there are some statistics about the filtering process in JSON format.  We typically discard those files with an acceptance ratio (well aligned words / all words) below 33% (to be more conservative).
 
Some remarks about speed and efficiency of alignment processes:
In the alignment config file, there are three `PRUNE_X` variables. In the first attempt, the recogniser (tLrecognise) will use `PRUNE_1` options (i.e. `-m 5000`, that is, maximum 5K active hyps at each time step). If at the last time step it cannot reach the final state, it will use `PRUNE_2`, and then, `PRUNE_3`. As a reference, the alignment process is quite fast with `-m 5000`, RTF of 0.01, even less. We recommend to try with 5K, 10K and 20K. In some cases, like parliamentary data with long files, this value can be larger.

### Filtering heuristics explained

We apply three different filtering heuristics:

#### Word-level filtering by phoneme length
  - Idea: drop words that contain high-frame-consuming phonemes
  - Steps:
    1. Estimate mean and stdev of phoneme lengths (frame occupation) for the whole audio file.
    2. Define maximum phoneme length threshold: `min( mean + H_LEN_PARAM * stdev , GLOBAL_MAXIMUM_PHONEME_LENGTH=100 frames)`
    3. For each aligned word, if its mean phoneme length is above the threshold, it is dropped/filtered out (= considered as silence/noise in the resegmentation step).

#### Word-level filtering by alignment score
  - Idea: drop words whose normalised frame scores (logits) are below a threshold.
  - Steps:
    1. Estimate mean and stddev normalised word frame score for the whole audio file
    2. Define thresholds
      1. Define minimum normalised word frame score threshold: `max(mean_frame_score * H_SCORE_PARAM, 0) # We don't allow negative logits. Don't ask why - their alignments seem to be crap.`. Note: `threshold = mean - (H_SCORE_PARAM * stdev)` (assuming normal distribution) was discarded because it drops a lot of data.
    3. For each aligned word, if it has a normalised frame score below the threshold, it is dropped/filtered out (= considered as silence/noise in the resegmentation step). 

#### Re-segmentation and segment-level filtering
  - Idea: create acoustic model training segments using silences and dropped words as segment boundaries.
  - Steps:
    1. Join into segments consecutive words separated by less `MIN_SIL_TO_SEGMENT` (def: 0.05) seconds of silence or dropped words.
    2. For each resulting segment:
     1. Drop if it lasts less than `MIN_SEGMENT_LENGTH` (def: 0.5) seconds.
     2. Split in several chunks if it lasts more than `MAX_SEGMENT_LENGTH` (def: 20) seconds:
     3. Accumulate words and split when their length exceeds the `MAX_SEGMENT_LENGTH` (def: 20 seconds).

### Experimental feature: always return an alignment

There are some data sources that, due to its nature, cannot be aligned even with low prune parameter values. This is the case of parliamentary data, where we try to force-align large audio files (i.e. 6 hours) to their corresponding hansards - typically a non-verbatim speech transcript extracted from a HTML or PDF file containing a lot of metadata in the surroundings. In such scenarios, it's very likely that TLK's decoder (tLrecognise) cannot align the last input frame to the last HMM state inferred from the transcript (in other words, the decoder reaches the end on the audio signal, while stuck in the middle of the huge text stream). When this happens, the tLrecognise returns an error message and no alignment at all. 

With the experimental `--always-recognise option`, we can tell the decoder to return always the best alignment, even though it's not reaching the end of the HMM state sequence. If so, there would be parts of the alignment, specially at the end, containing obvious errors, but these can be easily detected and removed by the filtering heuristics

This option can be supplied to the tool via the config file. There are two possibilities:

- within the `$TLRECOGNISE_OPTS` variable: in this case, will be applied to all alignment attempts. But, as you can figure out, it will be executed only once!
- within the `$PRUNE_X` variables. This gives us the flexibility to, for instance, try to fully align the file with two diferrent prune parameters, and, if not achieved, then re-run with high prune and --always-recognise:


Alignment config file
```
(...)
TLRECOGNISE_OPTS="-s SP"
PRUNE_1="-m 5000"
PRUNE_2="-m 20000"
PRUNE_3="-m 5000 --always-recognise"
```

## asr-wer-rank.sh

Apply ASR to a list of audio/video samples and rank them by (normalized) wer/cer scores.

### Requirements

**tlk**

### Usage

```
./asr-wer-rank.sh <CONFIG_FILE> <MEDIAS_LIST> <REFERENCES_TXT> <OUT_DIR> -b <TLK_BIN_DIR> -t <TMP_DIR> -n
```
 
Where:  

- `<CONFIG_FILE>`: asr recognition configuration variables. Format:

```
LANGUAGE="en"
LM="/path/to/tlk_language_model"
LEXM="/path/to/tied_lexicon_model"
GRAPH="/path/to/search_graph"
AM="/path/to/acoustic_model"
DNN="/path/to/FF-DNN_model"
TLEXTRACT_OPTS="" # tLextract options (i.e. -C 15 -n)
TLRECOGNISE_OPTS="" # tLrecognise options (i.e. "-g 12 -w 0 -M 200 -m 7500 -b 140")
```

All variables have to be set, with the exception of exclusively `$LM` plus `$LEXM`, or `$GRAPH`.

- `<MEDIAS_LIST>`: list of video or audio files to be recognized and ranked, one file per line.
- `<REFERENCES_TXT>`: reference text of each video or audio file, one per line, in the same order as `<MEDIAS_LIST>`.
- `<OUT_DIR>`: output directory where the results of the ranking will be placed.
- `<TLK_BIN_DIR>`: TLK's build bin directory, as the tool uses the ASR toolkit to perform the recognition
- `<TMP_DIR>`: Where to store tmp files.
- `-n` option: tells the script to apply a basic text normalization to both recognition and reference (lowercase, remove punctuation).

This will generate the following files inside the output directory:
 
- `recognition.txt` -> Output of the recognition, one line per input sample. 
- `recognition.norm.txt` -> Normalized version of the recognition file.
- `references.norm.txt` -> Normalized version of the `<REFERENCES_TXT>` file
- `rank.cer.txt` -> CER values measured using the recognised and reference texts, one per line, given in the same order as `<MEDIAS_LST>`.
- `rank.cer.norm.txt` -> CER values measured using the normalized version of recognised and reference texts, one per line, given in the same order as `<MEDIAS_LST>`.
- `rank.wer.txt` -> WER values measured using the recognised and reference texts, one per line, given in the same order as `<MEDIAS_LST>`.
- `rank.wer.norm.txt` -> WER values measured using the normalized version of recognised and reference texts, one per line, given in the same order as `<MEDIAS_LST>`.

