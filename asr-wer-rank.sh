#!/bin/bash -e
# Joan Albert Silvestre

export LC_ALL="C.UTF-8"

function log {
  echo -e "[LL] [${HOSTNAME%%.*}] [$(date "+%F %T")] $1"
}

function time_stats {
  begin=$1; end=$2; ref=$3
  diff=$(echo "$end - $begin" | bc -l | awk -F'.' '{print $1}') 
  time=$(date -u -d @${diff} +"%T")
  if [ $diff -ge 86400 ]; then
    time="$time(+$[diff/86400]days)"
  fi
  if [ "$ref" != "0" ]; then
    rtf=$(printf %.2f $(echo "$diff/$ref" | bc -l))
  else
    rtf="N/A"
  fi
}

function error {
  echo -e "[EE] [${HOSTNAME%%.*}] $1" >&2
  exit 1
}

function warn {
  echo -e "[WW] [${HOSTNAME%%.*}] $1" >&2
}

PRGNAME=$(basename $0)
NARGS=4

# Show help
function print_help {
    TO=$1
    SHOWHELP=$2
    echo "$PRGNAME [options] <CONFIG_FILE> <MEDIAS_LIST> <REFERENCES_TXT> <OUT_DIR>" > $TO
    echo "" > $TO
    [ $SHOWHELP -eq 0 ] && return
    cat <<EOF > $TO

  Options:

    -h: Shows this help
    -S <dir>: Scripts dir. Default: dirname of this script.
    -t <dir>: Set tmp directory. Default: /home/tmp/ttp/\$USER.\${JOB_ID}.\$JOB_NAME
    -p <dir>: Set tmp directory prefix. Default: /home/tmp/ttp
    -N      : Do not remove tmp dir at exit.
    -b <dir>: Set TLK bin directory. Default: extracted from \$PATH env variable.
    -n      : apply a generic text normalisation (lowercase, remove punct, etc.) procedure to both reference and hypothesis text
 
EOF
}

echo "*******************************************************************"
echo "*"
echo "* $USER@${HOSTNAME%%.*}"
echo "*"
echo "* $0 $@"
echo "*"
echo "*******************************************************************"

set +e
ffmpeg=$(which ffmpeg)
[ -z $ffmpeg ] && ffmpeg=$(which avconv)
[ -z $ffmpeg ] && error "ffmpeg/avconv not found in PATH"
ffprobe=$(which ffprobe)
[ -z $ffprobe ] && ffmpeg=$(which probe)
[ -z $ffprobe ] && error "ffprobe/avprobe not found in PATH"
[ "$(which tLrecognise)" != "" ] && TLK_BIN_DIR=$(dirname $(which tLrecognise)) || TLK_BIN_DIR=""
set -e
SCRIPTS_DIR="$(readlink -f $(dirname $0))"
TMP="NULL"
DELETE_TMP=1
TMP_PREF="/home/tmp/ttp"
NORM=0

ARGS=`getopt -o "hNt:b:np:S:" -n $PRGNAME -- "$@"`
eval set -- "$ARGS"
while true
do
    case $1 in
	-h) print_help /dev/stdout 1; exit 0;;
        -S) SCRIPTS_DIR="$(readlink -f $2)";;
        -t) TMP="$2";;
        -p) TMP_PREF="$2";;
        -N) DELETE_TMP=0;;
        -b) TLK_BIN_DIR="$(readlink -f $2)";;
        -n) NORM=1;;
	--) shift; break;;
    esac
    shift
done
[ $# != $NARGS ] && { print_help /dev/stderr 0; error "wrong number of arguments"; }

log "$(date)"
ts_g_s=$(date +%s)

CONFIG="$(readlink -f $1)"
MEDIA_LST="$(readlink -f $2)"
REFS_TXT="$(readlink -f $3)"
OUTDIR="$4"

mkdir -p "$OUTDIR"

[ ! -z "$JOB_ID" ] && MY_JOB_ID="$JOB_ID" || MY_JOB_ID=$$
[ "$TMP" = "NULL" ] && TMP="$TMP_PREF/asr-wer-rank.$USER.${MY_JOB_ID}"

[ -z "$TLK_BIN_DIR" ] && error "TLK binaries not found in \$PATH (use -b option?)"

TLK_TLTRECO_SCRIPTS="$TLK_BIN_DIR/../share/tlk/tLtask-recognise/scripts"
export PATH=$TLK_TLTRECO_SCRIPTS:$TLK_BIN_DIR:$PATH

if [ ! -s "$OUTDIR/recognition.txt" ]; then
    
    source "$CONFIG"
    [ ! -z "$LANGUAGE" ] && [ ! -z "$AM" ] &&  [ ! -z "$DNN" ] && 
       [ ! -z "$TLEXTRACT_OPTS" ] && [ ! -z "$TLRECOGNISE_OPTS" ] ||
       error "Missing required variables in the Config File"
    
    [ ! -z "$GRAPH" ] || [ ! -z "$LM" -a ! -z "$LEXM" ] || error "Must provide \$GRAPH or \$LM and \$LEXM"
    [ ! -z "$GRAPH" ] && [ ! -z "$LM" -a ! -z "$LEXM" ] && error "Must provide \$GRAPH or \$LM and \$LEXM"
    
    if [ ! -z "$GET_CACHE" ]; then
      log "Caching files..."
      log "$(date)"
      ts_ca_s=$(date +%s)
      if [ ! -z "$GRAPH" ]; then 
        GRAPH=$($GET_CACHE "$GRAPH")
      else
        LM=$($GET_CACHE "$LM")
        LEXM=$($GET_CACHE "$LEXM")
      fi
      DNN=$($GET_CACHE "$DNN")
      AM=$($GET_CACHE "$AM")
      ts_ca_e=$(date +%s)
    else
      ts_ca_s=0
      ts_ca_e=0
    fi
    
    mkdir -p "$TMP"
    #cd "$TMP"
    
    function finish {
       log "Removing TMP dir ($TMP)"
       rm -rf "$TMP"
    }
    
    if [ $DELETE_TMP -eq 1 ]; then
      trap finish 0 1 2 3 6 9 14 15
    #  trap finish EXIT
    fi
    
    set -e
    
    echo "**** TMP dir: $TMP ****"
    
    ts_pr_s=$(date +%s)
    # Extract audio
    mkdir -p "$TMP/wav"
    
    rm -f "$TMP/wavs.lst"
    rm -f "$TMP/trans.txt"
    
    if [ $(cat $MEDIA_LST | wc -l) -ne $(cat $REFS_TXT | wc -l) ]; then
      error "Number of lines of <MEDIAS_LST> and <REFERENCES_TXT> files differ"
    fi
    
    log "Computing all media duration for RTF statistics"
    vlen=0
    while read mf; do
      [ ! -s $mf ] && error "$mf doesn't exist"
      if [ ! -z "$(which $ffprobe)" ]; then
        x=$($ffprobe "$mf" -show_format_entry duration 2> /dev/null | grep "[0-9]\.[0-9]" | sed -e 's|duration=||g' | awk '{printf("%d", $1)}')
        [ $? -ne 0 ] && echo 0
        vlen=$[vlen+x]
      fi
    done < $MEDIA_LST 
    
   
    i=1 
    log "Extracting WAV"
    cat $MEDIA_LST | while read mf; do
      id=$(printf "%06d" $i)
      #wavf="$TMP/wav/$(basename $mf).wav"
      wavf="$TMP/wav/$id.wav"
      i=$[i+1]
      if [ ${mf##*.} = "wav" ]; then
        ln -s $mf $wavf
      else
         $ffmpeg -i "$mf" "$wavf" < /dev/null #&> /dev/null
      fi
      echo $wavf >> "$TMP/wavs.lst"
    done
    
    log "Extracting Features"
    mkdir -p "$TMP/feas"
    cat "$TMP/wavs.lst" | sed -e "s|$TMP/wav/|$TMP/feas/|g" -e 's|\.wav$|\.tLfea|g' > "$TMP/feas.lst"
    tLextract $TLEXTRACT_OPTS "$TMP/wavs.lst" "$TMP/feas.lst"
    
    ts_pr_e=$(date +%s)
    
    log "Recognition"
    
    ts_al_s=$(date +%s)
    
    nvidia-smi
    log "CUDA set by SGE: $CUDA_VISIBLE_DEVICES"
    
    LM_OPTS="$GRAPH"
    [ -z "$GRAPH" ] && LM_OPTS="-l $LEXM $LM"
    
    cmd="tLrecognise $TLRECOGNISE_OPTS -v -v -d $DNN -o $OUTDIR/recognition.txt $LM_OPTS $AM $TMP/feas.lst"
    
    log $cmd
    $cmd
    
    ts_al_e=$(date +%s)

else
  
    log "Output recognition file $OUTDIR/recognition.txt already exists!"
  
    ts_pr_s=0
    ts_pr_e=0
    ts_ca_s=0
    ts_ca_e=0
    ts_al_s=0
    ts_al_e=0

fi
    
ts_po_s=$(date +%s)

log "Computing WER/CER at sample level"

tmph=$(mktemp); 
tmpr=$(mktemp); 

paste -d '|' $OUTDIR/recognition.txt $REFS_TXT | while IFS='|' read hyp ref; do 
  echo $hyp > $tmph; 
  echo $ref > $tmpr; 
  $SCRIPTS_DIR/scripts/wer++.py --cer $tmph $tmpr | awk '{print $2}'; 
done > $OUTDIR/rank.cer.txt

paste -d '|' $OUTDIR/recognition.txt $REFS_TXT | while IFS='|' read hyp ref; do 
  echo $hyp > $tmph; 
  echo $ref > $tmpr; 
  $SCRIPTS_DIR/scripts/wer++.py $tmph $tmpr | awk '{print $2}'; 
done > $OUTDIR/rank.wer.txt

if [ $NORM -eq 1 ]; then
  log "Computing normalized WER/CER at sample level"
  norm_hyps="$OUTDIR/recognition.norm.txt"
  cat $OUTDIR/recognition.txt | python3 $SCRIPTS_DIR/scripts/txt-norm.py -s > "$norm_hyps"

  norm_refs="$OUTDIR/references.norm.txt"
  cat $REFS_TXT | python3 $SCRIPTS_DIR/scripts/txt-norm.py -s > "$norm_refs"

  paste -d '|' $norm_hyps $norm_refs | while IFS='|' read hyp ref; do 
    echo $hyp > $tmph; 
    echo $ref > $tmpr; 
    $SCRIPTS_DIR/scripts/wer++.py --cer $tmph $tmpr | awk '{print $2}'; 
  done > $OUTDIR/rank.norm.cer.txt
  
  paste -d '|' $norm_hyps $norm_refs | while IFS='|' read hyp ref; do 
    echo $hyp > $tmph; 
    echo $ref > $tmpr; 
    $SCRIPTS_DIR/scripts/wer++.py $tmph $tmpr | awk '{print $2}'; 
  done > $OUTDIR/rank.norm.wer.txt
fi

rm $tmph
rm $tmpr


ts_po_e=$(date +%s)

log "$(date)"

ts_g_e=$(date +%s)

# Compute stats
log "Computing time stats"
log "$(date)"
set +e

export LC_NUMERIC="en_US.UTF-8"

echo -e "\n----------------------------------"
echo -e "\tTIME\tRTF"
time_stats $ts_ca_s $ts_ca_e $vlen
echo -e "Cache\t$time\t$rtf"
time_stats $ts_pr_s $ts_pr_e $vlen
echo -e "Prepro\t$time\t$rtf"
time_stats $ts_al_s $ts_al_e $vlen
echo -e "Align\t$time\t$rtf"
time_stats $ts_po_s $ts_po_e $vlen
echo -e "Postpr\t$time\t$rtf"
time_stats $ts_g_s $ts_g_e $vlen
echo -e "TOTAL\t$time\t$rtf"
echo -e "----------------------------------\n"

log "$(date)"


