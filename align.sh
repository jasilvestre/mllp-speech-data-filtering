#!/bin/bash -e
# Joan Albert Silvestre - Oct18

export LC_ALL="C.UTF-8"

function log {
  echo -e "[LL] [${HOSTNAME%%.*}] [$(date "+%F %T")] $1"
}

function recover_slashes {
  sed -e 's|\([^_ ]*\)___\([^_ ]*\)|\1 \2|g' -e 's?[[]\([^|]*\)|\([^]]*\)[]]?/\1/\2/?g'
}

function time_stats {
  begin=$1; end=$2; ref=$3
  diff=$(echo "$end - $begin" | bc -l | awk -F'.' '{print $1}') 
  time=$(date -u -d @${diff} +"%T")
  if [ $diff -ge 86400 ]; then
    time="$time(+$[diff/86400]days)"
  fi
  if [ "$ref" != "0" ]; then
    rtf=$(printf %.2f $(echo "$diff/$ref" | bc -l))
  else
    rtf="N/A"
  fi
}

function error {
  echo -e "[EE] [${HOSTNAME%%.*}] $1" >&2
  exit 1
}

function warn {
  echo -e "[WW] [${HOSTNAME%%.*}] $1" >&2
}

PRGNAME=$(basename $0)
NARGS=4

# Show help
function print_help {
    TO=$1
    SHOWHELP=$2
    echo "$PRGNAME [options] <CONFIG_FILE> <IN_MEDIA> <SEGMENTS_LIST> <OUT_DIR>" > $TO
    echo "" > $TO
    [ $SHOWHELP -eq 0 ] && return
    cat <<EOF > $TO

  Options:

    -h: Shows this help
    -S <dir>: Scripts dir. Default: dirname of this script.
    -t <dir>: Set tmp directory. Default: /home/tmp/ttp/\$USER.\${JOB_ID}.\$JOB_NAME
    -p <dir>: Set tmp directory prefix. Default: /home/tmp/ttp
    -N      : Do not remove tmp dir at exit.
    -b <dir>: Set TLK bin directoty. Default: extracted from \$PATH env variable.
    -n <str>: Job name. Default: basename of input media file.
    -T      : Generate alignment for training purposes.
    -R      : Do not re-segment; use recognition segmentation instead.
    -l      : Deal with slash notation (/acoustic/correct/)
    -s      : Put acoustic silences at the beggining and at the end of the end of each segment to be aligned.
    -P <str>: Override prune options from config file. Example: "-p '-m 5000:-m 10000:-m 20000'"
 
EOF
}

echo "*******************************************************************"
echo "*"
echo "* $USER@${HOSTNAME%%.*}"
echo "*"
echo "* $0 $@"
echo "*"
echo "*******************************************************************"

set +e
ffmpeg=$(which ffmpeg)
[ -z $ffmpeg ] && ffmpeg=$(which avconv)
[ -z $ffmpeg ] && error "ffmpeg/avconv not found in PATH"
ffprobe=$(which ffprobe)
[ -z $ffprobe ] && ffmpeg=$(which probe)
[ -z $ffprobe ] && error "ffprobe/avprobe not found in PATH"
[ "$(which tLtranscribe)" != "" ] && TLK_BIN_DIR=$(dirname $(which tLtranscribe)) || TLK_BIN_DIR=""
set -e
SCRIPTS_DIR="$(readlink -f $(dirname $0))"
TMP="NULL"
MY_JOB_NAME="NULL"
DELETE_TMP=1
TRAIN=0
RESEG=1
TMP_PREF="/home/tmp/ttp"
MAX_OOVS_PERCENT=30
PARSE_SLASHES=0
SPS=0
PRUNE_OVR_OPTS="NULL"

ARGS=`getopt -o "ThNt:b:n:p:S:c:RlsP:" -n $PRGNAME -- "$@"`
eval set -- "$ARGS"
while true
do
    case $1 in
	-h) print_help /dev/stdout 1; exit 0;;
        -S) SCRIPTS_DIR="$(readlink -f $2)";;
        -t) TMP="$2";;
        -p) TMP_PREF="$2";;
        -N) DELETE_TMP=0;;
        -b) TLK_BIN_DIR="$(readlink -f $2)";;
        -n) MY_JOB_NAME="$2";;
        -T) TRAIN=1;;
        -R) RESEG=0;;
        -l) PARSE_SLASHES=1;;
        -s) SPS=1;;
        -P) PRUNE_OVR_OPTS="$2";;
	--) shift; break;;
    esac
    shift
done
[ $# != $NARGS ] && { print_help /dev/stderr 0; error "wrong number of arguments"; }

log "$(date)"
ts_g_s=$(date +%s)

CONFIG="$(readlink -f $1)"
IN_MEDIA="$(readlink -f $2)"
SEGMENTS_LIST="$(readlink -f $3)"
OUTDIR="$4"

mkdir -p "$OUTDIR"

[ "$MY_JOB_NAME" = "NULL" ] && [ ! -z "$JOB_NAME" ] && MY_JOB_NAME="$JOB_NAME" || MY_JOB_NAME="$(basename $IN_MEDIA)"
[ ! -z "$JOB_ID" ] && MY_JOB_ID="$JOB_ID" || MY_JOB_ID=$$
[ "$TMP" = "NULL" ] && TMP="$TMP_PREF/$USER.${MY_JOB_ID}.$MY_JOB_NAME"

TLK_TLTRECO_SCRIPTS="$TLK_BIN_DIR/../share/tlk/tLtask-recognise/scripts"
export PATH=$TLK_TLTRECO_SCRIPTS:$TLK_BIN_DIR:$PATH

source "$CONFIG"
[ ! -z "$LANGUAGE" ] && [ ! -z "$TIED_LIST" ] && [ ! -z "$AM" ] && 
   [ ! -z "$DNN" ] && [ ! -z "$TLEXTRACT_OPTS" ] && 
   [ ! -z "$W2P" ] && [ ! -z "$TLRECOGNISE_OPTS" ] ||
   error "Missing required variables in the Config File"

if [ $SPS -eq 1 ]; then
  [ ! -z "$SP_TOKEN" ] || error "Missing \$SP_TOKEN variable in config file"
fi

if [ $PRUNE_OVR_OPTS != "NULL" ]; then
  PRUNE_1=$(echo $PRUNE_OVR_OPTS | cut -d ':' -f 1) 
  PRUNE_2=$(echo $PRUNE_OVR_OPTS | cut -d ':' -f 2) 
  PRUNE_3=$(echo $PRUNE_OVR_OPTS | cut -d ':' -f 3) 
fi

lexmono2tri=$(which lexmono2tri.py)
align2dfxp=$(which tLoutalign2dfxp.py)
wgraph=$(which wgraph.py)

if [ ! -z "$GET_CACHE" ]; then
  log "Caching files..."
  log "$(date)"
  ts_ca_s=$(date +%s)
  TIED_LIST=$($GET_CACHE "$TIED_LIST")
  DNN=$($GET_CACHE "$DNN")
  AM=$($GET_CACHE "$AM")
  ts_ca_e=$(date +%s)
else
  ts_ca_s=0
  ts_ca_e=0
fi

mkdir -p "$TMP"
#cd "$TMP"

function finish {
   log "Removing TMP dir ($TMP)"
   rm -rf "$TMP"
}

if [ $DELETE_TMP -eq 1 ]; then
  trap finish 0 1 2 3 6 9 14 15
#  trap finish EXIT
fi

set -e

echo "**** TMP dir: $TMP ****"

ts_pr_s=$(date +%s)
# Extract audio
mkdir -p "$TMP/wav"

rm -f "$TMP/wavs.lst"
rm -f "$TMP/trans.txt"

n=0
cat $SEGMENTS_LIST | while read st et txt; do
  n=$[n+1]
  id="${st}_${et}-${n}"
  log "Processing Sample ID $id"
  len=$(echo "$et-$st" | bc -l | awk '{printf "%f", $0}')
  wavf="$TMP/wav/$id.wav"
  $ffmpeg -i "$IN_MEDIA" -ss "$st" -t "$len" "$TMP/wav/$id.wav" < /dev/null #&> /dev/null
  echo $wavf >> "$TMP/wavs.lst"
  if [ $SPS -eq 1 ]; then
    echo "@@@${SP_TOKEN}@@@ $txt @@@${SP_TOKEN}@@@" >> "$TMP/trans.txt"
  else
    echo "$txt" >> "$TMP/trans.txt"
  fi
done

# Generate Lexicon
log "Generating Lexicon"

if [ $PARSE_SLASHES -eq 1 ]; then

  set +e
  
  # 1) Python: strip invisible characters
  # 2) sed: convert slash notation /// to [|] for simplicity, and strip blank characters inside regexps
  # 3) awk: convert inner blank spaces to "___"
  # 4) sed: join punctuation characters to nearest words
  cat $TMP/trans.txt | while read sent; do
    ret=$(echo " $sent " |
    python -c "import sys; print u' '.join(sys.stdin.read().decode('utf-8').split()).encode('utf-8')" | 
    sed -e 's?/\([^/]*\)/\([^/]*\)/?[\1|\2]?g' \
        -e 's?[[] *?[?g' \
        -e 's? *| *?|?g' \
        -e 's? *[]]?]?g' |
    awk 'BEGIN {state=0}; 
    {
      ret="";
      for(i=1;i<=NF;++i){
        if(state==0&&$i~/[[]/){state=1;}
        if(state==1&&$i~/[]]/){state=0;}
        if(state==0)ret=ret $i " "
        else ret=ret $i "___"
      }
      print ret
    }' | 
    sed -e 's|\([^[:space:]]\{1,\}\)[[:space:]]\{1,\}\([[:punct:]]\{1,\}\)\([[:space:]]\{1,\}\)| \1\2\3 |g' \
        -e 's|\([[:space:]]\)\{1,\}\([[:punct:]]\{1,\}\)[[:space:]]\{1,\}\([^[:space:]]\{1,\}\)| \1\2\3 |g' \
        -e 's|^\([[:punct:]]\{1,\}\)[[:space:]]\{1,\}\([^[:space:]]\{1,\}\)| \1\2 |g' \
        -e 's|\([^[:space:]]\{1,\}\)[[:space:]]\{1,\}\([[:punct:]]\{1,\}\)[[:space:]]\{0,\}$| \1\2 |g' \
        -e 's|\([[:punct:]]\{1,\}\)[[:space:]]\{1,\}\([[:punct:]]\{1,\}\)\([[:space:]]\{1,\}\)\([[:punct:]]\{1,\}\)|\1\2\3\4|g' \
        -e 's|[[:space:]]\{1,\}\([[:punct:]]\{1,\}\)\([[:space:]]\{1,\}\)|\1\2|g' \
        -e 's|[[:space:]]\{1,\}\([[:punct:]]\{1,\}\)\([[:space:]]\{1,\}\)|\1\2|g' \
        -e 's|[[:space:]]\{1,\}\([[:punct:]]\{1,\}\)\([[:space:]]\{1,\}\)|\1\2|g' \
        -e 's|[[:space:]]\{1,\}\([[:punct:]]\{1,\}\)\([[:space:]]\{1,\}\)|\1\2|g')
    echo "$ret"
  done > "$TMP/trans.preproc.txt"
  
  [ $SPS -eq 1 ] && cat "$TMP/trans.preproc.txt" | sed -e "s|\([^ ]*\)\(@@@${SP_TOKEN}@@@\)\([^ ]*\)|\1 \2 \3|g" > "$TMP/trans.preproc.txt.2" && mv "$TMP/trans.preproc.txt.2" "$TMP/trans.preproc.txt"

  awk '{for (i=1; i<=NF; ++i) d[$i]=0;} END {for(k in d) print k}' "$TMP/trans.preproc.txt" | sort > "$TMP/trans.vocab.txt"

  [ $SPS -eq 1 ] && grep -v "^@@@${SP_TOKEN}@@@$" "$TMP/trans.vocab.txt" > "$TMP/trans.vocab.txt.2" && mv "$TMP/trans.vocab.txt.2" "$TMP/trans.vocab.txt"

  cat "$TMP/trans.vocab.txt" | sed -e 's/[[]\([^|]*\)|\([^]]*\)[]]/\1/g' > "$TMP/trans.vocab.clean.txt"
  grep -nvxF -f "$TMP/trans.vocab.clean.txt" "$TMP/trans.vocab.txt" > "$TMP/trans.vocab.diff.txt"
  
  nvoc=$(cat "$TMP/trans.vocab.txt" | wc -l)
  [ $nvoc -eq 0 ] && error "Empty vocabulary! Aborting." 
  
  $W2P "$TMP/trans.vocab.clean.txt" > "$TMP/mono.lex"
  
  # Recover slash notation /// (but converted to [|] for simplicity) to the monolex
  if [ $(cat "$TMP/trans.vocab.diff.txt" | wc -l) -gt 0 ]; then
    cp "$TMP/mono.lex" "$TMP/mono.lex.bak"
    cat "$TMP/trans.vocab.diff.txt" | while read x; do
      line=$(echo $x | awk -F ':' '{print $1}')
      mysed=$(paste <(sed "${line}q;d" "$TMP/trans.vocab.clean.txt") <(sed "${line}q;d" "$TMP/trans.vocab.txt") | 
        awk '{
               gsub("\"", "\\\"", $1); 
               gsub("\"", "\\\"", $2); 
               gsub("@", "\\@", $1); 
               gsub("@", "\\@", $2); 
               print "s@^"$1" 0@"$2" 0@"
             }')
      warn "Seding mono lex ---->    $mysed"
      grep "^$(sed "${line}q;d" "$TMP/trans.vocab.clean.txt") 0" "$TMP/mono.lex" | sed -e "$mysed" >> "$TMP/mono.lex"
      #sed -e "$mysed" "$TMP/mono.lex" > "$TMP/mono.lex.new" && mv "$TMP/mono.lex.new" "$TMP/mono.lex"
    done
  fi

else

  # 1) Python: strip invisible characters
  # 2) Sed: join punctuation characters to nearest words
  cat $TMP/trans.txt | while read sent; do
    ret=$(echo " $sent " |
    python -c "import sys; print u' '.join(sys.stdin.read().decode('utf-8').split()).encode('utf-8')" | 
    sed -e 's|\([^[:space:]]\{1,\}\)[[:space:]]\{1,\}\([[:punct:]]\{1,\}\)\([[:space:]]\{1,\}\)| \1\2\3 |g' \
        -e 's|\([[:space:]]\)\{1,\}\([[:punct:]]\{1,\}\)[[:space:]]\{1,\}\([^[:space:]]\{1,\}\)| \1\2\3 |g' \
        -e 's|^\([[:punct:]]\{1,\}\)[[:space:]]\{1,\}\([^[:space:]]\{1,\}\)| \1\2 |g' \
        -e 's|\([^[:space:]]\{1,\}\)[[:space:]]\{1,\}\([[:punct:]]\{1,\}\)[[:space:]]\{0,\}$| \1\2 |g' \
        -e 's|\([[:punct:]]\{1,\}\)[[:space:]]\{1,\}\([[:punct:]]\{1,\}\)\([[:space:]]\{1,\}\)\([[:punct:]]\{1,\}\)|\1\2\3\4|g' \
        -e 's|[[:space:]]\{1,\}\([[:punct:]]\{1,\}\)\([[:space:]]\{1,\}\)|\1\2|g' \
        -e 's|[[:space:]]\{1,\}\([[:punct:]]\{1,\}\)\([[:space:]]\{1,\}\)|\1\2|g' \
        -e 's|[[:space:]]\{1,\}\([[:punct:]]\{1,\}\)\([[:space:]]\{1,\}\)|\1\2|g' \
        -e 's|[[:space:]]\{1,\}\([[:punct:]]\{1,\}\)\([[:space:]]\{1,\}\)|\1\2|g')
    echo "$ret"
  done > "$TMP/trans.preproc.txt"

  [ $SPS -eq 1 ] && cat "$TMP/trans.preproc.txt" | sed -e "s|\([^ ]*\)\(@@@${SP_TOKEN}@@@\)\([^ ]*\)|\1 \2 \3|g" > "$TMP/trans.preproc.txt.2" && mv "$TMP/trans.preproc.txt.2" "$TMP/trans.preproc.txt"
  
  awk '{for (i=1; i<=NF; ++i) d[$i]=0;} END {for(k in d) print k}' "$TMP/trans.preproc.txt" | sort > "$TMP/trans.vocab.txt"

  [ $SPS -eq 1 ] && grep -v "^@@@${SP_TOKEN}@@@$" "$TMP/trans.vocab.txt" > "$TMP/trans.vocab.txt.2" && mv "$TMP/trans.vocab.txt.2" "$TMP/trans.vocab.txt"

  nvoc=$(cat "$TMP/trans.vocab.txt" | wc -l)
  [ $nvoc -eq 0 ] && error "Empty vocabulary! Aborting." 

  $W2P "$TMP/trans.vocab.txt" > "$TMP/mono.lex"

fi
 
# Check untranscribed words
cat "$TMP/mono.lex" | awk '{ if($1 != "LEXICON" && NF<3) print $1}' > "$TMP/mono.untranscribed.txt"
notxs=$(cat "$TMP/mono.untranscribed.txt" | wc -l)

if [ $notxs -gt 0 ]; then
  perc_notxs=$(echo "$notxs $nvoc" | awk '{printf("%d", $1/$2*100)}')
  if [ $perc_notxs -ge $MAX_OOVS_PERCENT ]; then
    error "Found a lot of untranscribed words ($notxs/$nvoc = $perc_notxs%) in the generated monophone lexicon. See: '$TMP/mono.untranscribed.txt'. Aborting."
  else
    warn "Some untranscribed words ($notxs/$nvoc = $perc_notxs%) found in the generated monophone lexicon:"
    cat "$TMP/mono.untranscribed.txt" >&2
    cat "$TMP/mono.lex" | awk '{ if($1 != "LEXICON" && NF<3) print $1" "$2" SP"; else print;}' > "$TMP/mono.fixed.lex"
  fi
else
  CDIR=$PWD
  cd $TMP
  ln -s "mono.lex" "mono.fixed.lex"
  cd $CDIR
fi

# Check omitted (OOVs) words
cat "$TMP/mono.fixed.lex" | grep -v "^LEXICON$" | awk '{print $1}' | sort > "$TMP/mono.fixed.vocab.txt"
grep -vxF -f "$TMP/mono.fixed.vocab.txt" "$TMP/trans.vocab.txt" | sort > "$TMP/mono.fixed.out-of-vocab.txt"
oovs=$(cat "$TMP/mono.fixed.out-of-vocab.txt" | wc -l)

if [ $oovs -gt 0 ]; then
  perc_oovs=$(echo "$oovs $nvoc" | awk '{printf("%d", $1/$2*100)}')
  if [ $perc_oovs -ge $MAX_OOVS_PERCENT ]; then
    error "Found a lot of OOVs ($oovs/$nvoc = $perc_oovs%) in the generated monophone lexicon. See: '$TMP/mono.fixed.out-of-vocab.txt'. Aborting."
  else
    warn "Some OOVs ($oovs/$nvoc = $perc_oovs%) found in the generated monophone lexicon:"
    cat "$TMP/mono.fixed.out-of-vocab.txt" >&2
    cat "$TMP/mono.fixed.out-of-vocab.txt" | awk '{print $1" 0 SP"}' >> "$TMP/mono.fixed.lex"
  fi
else
  rm "$TMP/mono.fixed.vocab.txt" "$TMP/mono.fixed.out-of-vocab.txt"
fi

# Check untranscribed + OOVs
tperc_oovs=$(echo "$notxs $oovs $nvoc" | awk '{printf("%d", ($1+$2)/$3*100)}')
if [ $tperc_oovs -ge $MAX_OOVS_PERCENT ]; then
  error "Found a lot of untranscribed + OOVs ( ($notx+$oovs)/$nvoc = $tperc_oovs%) in the generated monophone lexicon. Aborting."
fi


if [ $SPS -eq 1 ]; then
  echo "@@@${SP_TOKEN}@@@ 0 $SP_TOKEN" >> "$TMP/mono.fixed.lex"
fi

cat "$TMP/mono.fixed.lex" | $lexmono2tri $LEXMONO2TRI_OPTS "$TIED_LIST" > "$TMP/tied.lex"

set -e

# Feature Extraction
log "Extracting Features"

mkdir -p "$TMP/feas"

cat "$TMP/wavs.lst" | sed -e "s|$TMP/wav/|$TMP/feas/|g" -e 's|\.wav$|\.tLfea|g' > "$TMP/feas.lst"

tLextract $TLEXTRACT_OPTS "$TMP/wavs.lst" "$TMP/feas.lst"

ts_pr_e=$(date +%s)

# Alignment
log "Aligning Features"

ts_al_s=$(date +%s)

did_align=0

if [ $TRAIN -eq 1 ]; then
  mkdir -p "$TMP/wgs"
  cat "$TMP/wavs.lst" | sed -e "s|$TMP/wav/|$TMP/wgs/|g" -e 's|\.wav$|\.tLwg|g' > "$TMP/wgs.lst"
  TLRECOGNISE_OPTS="$TLRECOGNISE_OPTS -W $TMP/wgs.lst -N 1 --htk-wgs"
fi

for i in 1 2 3 ; do
    cprune=$(echo $(eval "echo $""PRUNE_$i"))
    if [ -z "$cprune" ]; then
      if [ $i -eq 1 ]; then 
        error "\$PRUNE_$i variable was not set on config file ($CONFIG)"
      else
        warn "\$PRUNE_$i variable was not set on config file ($CONFIG)"
        continue
      fi
    fi

    #source $SCRIPTS_DIR/set_best_gpu.sh # export CUDA_VISIBLE_DEVICES var with the most free GPU
    #$SCRIPTS_DIR/set_best_gpu.sh

    nvidia-smi
    log "CUDA set by SGE: $CUDA_VISIBLE_DEVICES"

    log "- Using prune LEVEL $i ($cprune) ..."
    log "tLrecognise $TLRECOGNISE_OPTS $cprune -v -v --sym-align --lm-align -l $TMP/tied.lex -d $DNN -o $TMP/align.txt $TMP/trans.preproc.txt $AM $TMP/feas.lst"
    tLrecognise $TLRECOGNISE_OPTS $cprune -v -v --sym-align --lm-align -l "$TMP/tied.lex" -d "$DNN" -o "$TMP/align.txt" "$TMP/trans.preproc.txt" "$AM" "$TMP/feas.lst"

    if [ $(cat "$TMP/align.txt" | grep -A 1 '^"' | grep '^\.' | wc -l) -eq 1 ]; then
        warn "Could not align some samples using PRUNE LEVEL ${i}."
    else
        cp "$TMP/align.txt" "$OUTDIR/align.tlk"
        did_align=1
        break
    fi

done

ts_al_e=$(date +%s)

[ $did_align -eq 0 ] && error "Could not align some samples. Aborting."

ts_po_s=$(date +%s)


a2d_opts=""
if [ $RESEG -eq 1 ]; then
    if [ $TRAIN -eq 1 ]; then 
        while read wg; do
            cat $wg | $wgraph -A
        done < "$TMP/wgs.lst" > "$OUTDIR/align.scores.txt"
        a2d_opts="-T -p $OUTDIR/align.scores.txt"
        log "$align2dfxp -I $a2d_opts $TMP/align.txt"
        $align2dfxp -I $a2d_opts "$TMP/align.txt" > "$OUTDIR/align.filtered.dfxp" 
        $align2dfxp $a2d_opts "$TMP/align.txt" -S > "$OUTDIR/align.filtered.srt" 
        set +e
        $align2dfxp $a2d_opts "$TMP/align.txt" -W "$OUTDIR/align.stats.json" -J > "$OUTDIR/align.filtered.json" #&> /dev/null
        [ $? -ne 0 ] && $align2dfxp $a2d_opts "$TMP/align.txt" -J > "$OUTDIR/align.filtered.json" # FIXME: deprecated
        set -e
    fi
    a2d_opts=""
else
    a2d_opts="-n -t human"
fi

log "$align2dfxp $a2d_opts $TMP/align.txt"

$align2dfxp "$TMP/align.txt" $a2d_opts > "$OUTDIR/align.dfxp"
[ $SPS -eq 1 ] && cat "$OUTDIR/align.dfxp" | sed -e "s|@@@${SP_TOKEN}@@@||g" > "$OUTDIR/align.dfxp.2" && mv "$OUTDIR/align.dfxp.2" "$OUTDIR/align.dfxp"

$align2dfxp "$TMP/align.txt" $a2d_opts -S > "$OUTDIR/align.srt" 
[ $SPS -eq 1 ] && cat "$OUTDIR/align.srt" | sed -e "s|@@@${SP_TOKEN}@@@||g" > "$OUTDIR/align.srt.2" && mv "$OUTDIR/align.srt.2" "$OUTDIR/align.srt"

$align2dfxp "$TMP/align.txt" $a2d_opts -J > "$OUTDIR/align.json" 
[ $SPS -eq 1 ] && cat "$OUTDIR/align.json" | sed -e "s|@@@${SP_TOKEN}@@@||g" > "$OUTDIR/align.json.2" && mv "$OUTDIR/align.json.2" "$OUTDIR/align.json"

if [ $PARSE_SLASHES -eq 1 ]; then
  
  # Recover slash notation /// (but converted to [|] for simplicity) to the monolex
  if [ $(cat "$TMP/trans.vocab.diff.txt" | wc -l) -gt 0 ]; then
    cat "$OUTDIR/align.dfxp" | recover_slashes > "$OUTDIR/align.dfxp.new" && mv "$OUTDIR/align.dfxp.new" "$OUTDIR/align.dfxp"
    cat "$OUTDIR/align.srt" | recover_slashes > "$OUTDIR/align.srt.new" && mv "$OUTDIR/align.srt.new" "$OUTDIR/align.srt"
    cat "$OUTDIR/align.json" |
      python -c "import sys; import json; import re;
  d = json.loads(sys.stdin.read())
  for s in d:
    for w in s['wl']:
      w['w'] = re.sub(r'([^_ ]*)___([^_ ]*)', r'\1 \2', w['w'])
      w['w'] = re.sub(r'[[]([^|]*)\|([^]]*)[]]', r'/\1/\2/', w['w'])
  print json.dumps(d)" > "$OUTDIR/align.json.new" && mv "$OUTDIR/align.json.new" "$OUTDIR/align.json"
  fi
  
  #sed -e 's|\([^_ ]*\)___\([^_ ]*\)|\1 \2|g' -e 's?[[]\([^|]*\)|\([^]]*\)[]]?/\1/\2/?g'

fi
 
ts_po_e=$(date +%s)

log "$(date)"

ts_g_e=$(date +%s)

# Compute stats
log "Computing time stats"
log "$(date)"
set +e

if [ ! -z "$(which $ffprobe)" ]; then
  vlen=$($ffprobe "$IN_MEDIA" -show_format_entry duration | grep "[0-9]\.[0-9]" | sed -e 's|duration=||g')
  [ $? -ne 0 ] && vlen=0
else
  vlen=0 
fi

export LC_NUMERIC="en_US.UTF-8"

echo -e "\n----------------------------------"
echo -e "\tTIME\tRTF"
time_stats $ts_ca_s $ts_ca_e $vlen
echo -e "Cache\t$time\t$rtf"
time_stats $ts_pr_s $ts_pr_e $vlen
echo -e "Prepro\t$time\t$rtf"
time_stats $ts_al_s $ts_al_e $vlen
echo -e "Align\t$time\t$rtf"
time_stats $ts_po_s $ts_po_e $vlen
echo -e "Postpr\t$time\t$rtf"
time_stats $ts_g_s $ts_g_e $vlen
echo -e "TOTAL\t$time\t$rtf"
echo -e "----------------------------------\n"

log "$(date)"


