#!/bin/bash -e
# Joan Albert Silvestre - Oct18

# get required gpu memory by the process (SGE)
#aux=$(qstat -j $JOB_ID | grep "^hard resource_list" | sed -n "s/.*gpu_mem_free=\([0-9.]\+\)\([GMK]\).*/\1|\2/p")

[ -z $CUDA_VISIBLE_DEVICES ] && echo -e "\n\$CUDA_VISIBLE_DEVICES not set! Aborting.\n" 1>&2 && exit 1
echo "Info: CUDA_VISIBLE_DEVICES=$CUDA_VISIBLE_DEVICES" 1>&2


# get required gpu memory by the process (manual)
if [ $# -ne 1 ]; then
  echo -e "Usage: $0 <GPU_MEM>\nExample: $0 2.5G" 1>&2
  exit 1
fi
argmem=$1

[ $(echo $argmem | grep "[GMK]" | wc -l) -eq 0 ] && echo "Bad format for <GPU_MEM> (Given: $argmem; Example: 2.5G)" 1>&2 && exit 1

#LOCKFILE=/tmp/get_available_gpu_slots.lock
#echo -e "\nTrying to lock $LOCKFILE" 1>&2
#lockfile -3 -r 30 $LOCKFILE
#[ $? -eq 0 ] && echo -e "Locked!\n" 1>&2
#trap "echo -e '\nRemoving Lockfile $LOCKFILE' 1>&2 && rm -f $LOCKFILE" 0 1 2 3 6 9 14 15

aux=$(echo $argmem | sed -n "s/\([0-9.]\+\)\([GMK]\).*/\1|\2/p")
mq=${aux%|*}
mu=${aux#*|}
[ "$mu" = "G" ] && req_gpu_mem=$(echo $mq*1073741824 | bc -l | sed -e 's|\..*||g')
[ "$mu" = "M" ] && req_gpu_mem=$(echo $mq*1048576 | bc -l | sed -e 's|\..*||g')
[ "$mu" = "K" ] && req_gpu_mem=$(echo $mq*1024 | bc -l | sed -e 's|\..*||g')


nvidia-smi 1>&2
[ $(nvidia-smi --query-gpu=index --format=csv,noheader | grep "$CUDA_VISIBLE_DEVICES" | wc -l) -ne 1 ] && echo -e "\nGPU device #$CUDA_VISIBLE_DEVICES not found!" 1>&2 && exit 1

ret=$(nvidia-smi --query-gpu=uuid,memory.total,memory.free,name,index --format=csv,noheader | sed -e 's/, /|/g' | 
      awk -v reqm=$req_gpu_mem -v si=$CUDA_VISIBLE_DEVICES -F'|' 'function getbytes(q, u){
                                         if(u == "GiB") 
                                           return q*1073741824; 
                                         else if(u == "MiB") 
                                           return q*1048576; 
                                         else if(u == "KiB") 
                                           return q*1024; 
                                         else return q;}
   
                                         $5 == si { split($2,a," "); tmem=getbytes(a[1],a[2]);
                                                    split($3,b," "); amem=getbytes(b[1],b[2]);
                                                    ntheor = int(tmem/reqm);
                                                    nreal = int(amem/reqm);
                                                    printf("%s|%d|%s|%d|%d|%d|%d", $4, $5, $1, tmem, amem, ntheor, nreal); }')

echo -e "\nUsing device #$(echo $ret | cut -d '|' -f 2): $(echo $ret | cut -d '|' -f 1) ($(echo $ret | cut -d '|' -f 3))\n" 1>&2
echo -e "- Total memory: $(echo $ret | cut -d '|' -f 4) bytes" 1>&2
echo -e "- Available memory: $(echo $ret | cut -d '|' -f 5) bytes" 1>&2
echo -e "- Requested memory: $req_gpu_mem bytes" 1>&2
echo -e "- Slots (allowed/maximum): $(echo $ret | cut -d '|' -f 7)/$(echo $ret | cut -d '|' -f 6)" 1>&2
echo -e "" 1>&2

echo $(echo $ret | cut -d '|' -f 7)
