#!/bin/bash
# Joan Albert Silvestre - Sep18

  job_stats_f=$(mktemp)
  qstat -j $JOB_ID > $job_stats_f

  gpu_info_f=$(mktemp)
  nvidia-smi --query-gpu=uuid,memory.total,memory.free,name,index --format=csv,noheader | sed -e 's/, /|/g' > $gpu_info_f

  NVIDIA_SELECTED_GPU=$(awk -F '|' 'function getbytes(q, u){
                                      if(u == "GiB") 
                                        return q*1073741824; 
                                      else if(u == "MiB") 
                                        return q*1048576; 
                                      else if(u == "KiB") 
                                        return q*1024; 
                                      else return q;}
       
                                    BEGIN{ total_mem=0; max_avail_mem=0; id=-1; name="UNK"; } 

                                    { split($2,a," "); tmem=getbytes(a[1],a[2]);
                                      split($3,b," "); amem=getbytes(b[1],b[2]);
                                      if(amem > max_avail_mem && tmem > 1610612736) {
                                        max_avail_mem=amem; id=$1; name=$4; ind=$5; total_mem=tmem;
                                      }
                                    } 

                                    END{ print id"|"total_mem"|"max_avail_mem"|"name"|"ind; }' $gpu_info_f)

  NVIDIA_SELECTED_GPU_ID=$(echo $NVIDIA_SELECTED_GPU | cut -d '|' -f 1)
  NVIDIA_SELECTED_GPU_MAX_MEM=$(echo $NVIDIA_SELECTED_GPU | cut -d '|' -f 2)
  NVIDIA_SELECTED_GPU_AVAIL_MEM=$(echo $NVIDIA_SELECTED_GPU | cut -d '|' -f 3)
  NVIDIA_SELECTED_GPU_NAME=$(echo $NVIDIA_SELECTED_GPU | cut -d '|' -f 4)
  NVIDIA_SELECTED_GPU_INDEX=$(echo $NVIDIA_SELECTED_GPU | cut -d '|' -f 5)

  export "CUDA_VISIBLE_DEVICES=$NVIDIA_SELECTED_GPU_ID"

  nvidia-smi

  echo -e "\n\n[II] Setting GPU #$NVIDIA_SELECTED_GPU_INDEX: CUDA_VISIBLE_DEVICES=$NVIDIA_SELECTED_GPU_ID ($NVIDIA_SELECTED_GPU_NAME)\n"
  
  rm -f $gpu_info_f

  rm -f $job_stats_f

