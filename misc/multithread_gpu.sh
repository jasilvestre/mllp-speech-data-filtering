#!/bin/bash 
# Joan Albert Silvestre - Oct18

CDIR=$(readlink -f $(dirname $0))

if [ $# -ne 4 ]; then
  echo "$0 <TASK_ID> <MEM> <CMDS_LIST> <EXECUTED_CMDS_LIST>"
  exit 1
fi

task=$1
mem=$2
cmds=$3
progr=$4

echo -e "\n[II] [$(date)] Started!"

[ ! -e $progr ] && touch $progr

nslots=$($CDIR/get_available_gpu_slots.sh $mem)
[ $? -ne 0 ] && exit 1
[ $nslots -eq 0 ] && echo "[EE] No GPU slots available." && exit 1
echo "[II] Allowed to launch $nslots GPU processes."

LOCKFILE=/home/$USER/.multithread.$task.lock

echo -e "\n[II] [$(date)] Trying to lock $LOCKFILE" 1>&2
lockfile -5 -r 20 $LOCKFILE
[ $? -eq 0 ] && echo -e "[II] [$(date)] Locked!\n" 1>&2 || exit 1
trap "echo -e '\n[II] Removing Lockfile $LOCKFILE' && rm -f $LOCKFILE" EXIT

remaining=$(mktemp)
grep -vxFf $progr $cmds > $remaining
trap "rm -f $remaining" EXIT
[ $(cat $remaining | wc -w) -eq 0 ] && echo "[WW] No remaining jobs pending to submit" && rm -f $LOCKFILE && exit 0

pids=""
i=0
while read cmd; do
  i=$[i+1]
  echo -e "\n[II] Launching thread $i:\n"
  echo "$cmd"
  eval "$cmd" &
  pid=$!
  pids="$pids$pid "
  echo "PID: $pid"
  echo $cmd >> $progr
  [ $i -ge $nslots ] && break
done < $remaining

echo -e "\n[II] [$(date)] Removing Lockfile $LOCKFILE"
rm -f $LOCKFILE

echo -e "\n[II] [$(date)] Waiting jobs $pids"
wait $pids
echo -e "\n[II] [$(date)] Finished!"


exit 0
